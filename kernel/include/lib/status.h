#ifndef _STATUS_H_
#define _STATUS_H_

#include "lib/std.h"


typedef DWORD                                       STATUS;

#define SUCCESS(Status)                             (((STATUS)(Status)) == 0)

/* Very simple statuses */
#define STATUS_SUCCESS                              ((STATUS)0x00000000)
#define STATUS_SERIAL_ALREADY_INITIALIZED           ((STATUS)0x00000001)
#define STATUS_NO_SERIAL_PORT_AVAILABLE             ((STATUS)0x00000002)
#define STATUS_SERIAL_NOT_INITIALIZED               ((STATUS)0x00000003)
#define STATUS_INVALID_PARAMETER_1                  ((STATUS)0x00000004)
#define STATUS_NO_MEMORY_FOUND                      ((STATUS)0x00000004)
#define STATUS_PAGE_ALREADY_MAPPED                  ((STATUS)0x00000005)
#define STATUS_PAGE_STRUCRURE_CORRUPTED             ((STATUS)0x00000006)
#define STATUS_ACPI_FAIL                            ((STATUS)0x00000007)
#define STATUS_COPY_MEMORY_FAILED                   ((STATUS)0x00000008)
#define STATUS_NO_CPU_FOUND                         ((STATUS)0x00000009)
#define STATUS_VMX_NOT_SUPPORTED                    ((STATUS)0x0000000A)
#define STATUS_VMX_OPERATION_FAIL                   ((STATUS)0x0000000B)
#define STATUS_GDT_NOT_INITIALIZED                  ((STATUS)0x0000000C)
#define STATUS_IDT_NOT_INITIALIZED                  ((STATUS)0x0000000D)
#define STATUS_NOT_IMPLEMENTED                      ((STATUS)0x0000000E)
#define STATUS_NOT_SUPPORTED                        ((STATUS)0x0000000F)
#define STATUS_INVALID_PARAMETERS                   ((STATUS)0x00000010)
#define STATUS_INVALID_PARAMETER_2                  ((STATUS)0x00000011)
#define STATUS_DRIVER_ALREADY_EXIST                 ((STATUS)0x00000012)
#define STATUS_DRIVER_ALREADY_UNREGISTERED          ((STATUS)0x00000013)
#define STATUS_DRIVER_DO_NOT_EXIST                  ((STATUS)0x00000014)
#define STATUS_INVALID_PARAMETER_3                  ((STATUS)0x00000015)
#define STATUS_BITMAP_FULL                          ((STATUS)0x00000016)
#define STATUS_INVALID_PARAMETER_5                  ((STATUS)0x00000017)
#define STATUS_ADDRESS_ALREADY_MAPPED               ((STATUS)0x00000018)
#define STATUS_COMPONENT_NOT_INITIALIZED            ((STATUS)0x00000019)
#define STATUS_INVALID_PARAMETER_4                  ((STATUS)0x00000020)

#endif