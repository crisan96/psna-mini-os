#ifndef _CRT_H_
#define _CRT_H_

#include "lib/std.h"

/*1*/ void*     crt_memcpy(void *Dest, const void *Src, size_t Length);
/*2*/ size_t    crt_strlen(const CHAR* Str);
/*3*/ CHAR*     crt_itoa(INT64 Num, CHAR* Str, BYTE Base);
/*4*/ CHAR*     crt_uitoa(QWORD Num, CHAR* Str, BYTE Base);
/*6*/ void      crt_sprintf(CHAR* Buffer, const CHAR *Format, ...);
/*7*/ void*     crt_memset(void *Dest, int Value, size_t Length);
/*8*/ int       crt_strcmp(const char *string1, const char *string2);

#endif // !_CRT_H_
