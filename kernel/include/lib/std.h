#ifndef _MAIN_H_
#define _MAIN_H_

#include "lib/intrin.h"

#ifndef FALSE
#define FALSE                           (0)
#endif

#ifndef TRUE
#define TRUE                            (1)
#endif

#ifndef NULL
#define NULL                            0
#endif

#define UNREFERENCED_PARAMETER(X)   (X)

typedef char                CHAR, *PCHAR;    // To avoid "const BYTE *' differs in indirection to slightly different base types from 'char [N]"


//
// standard types - define them with explicit length
//
#ifdef _WIN64
typedef unsigned __int64    size_t;
#else
typedef unsigned __int32    size_t;
#endif

//
// default types
//
typedef unsigned __int8     BOOLEAN;
typedef unsigned __int8     BYTE, *PBYTE;
typedef unsigned __int16    WORD, *PWORD;
typedef unsigned __int32    DWORD, *PDWORD;
typedef unsigned __int64    QWORD, *PQWORD;
typedef signed __int8       INT8;
typedef signed __int16      INT16;
typedef signed __int32      INT32;
typedef signed __int64      INT64;

#define                     KILO            (1024)
#define                     MEGA            (1024 * 1024)
#define                     GIGA            ((QWORD)1024 * 1024 * 1024)
#define                     TERA            ((QWORD)1024 * 1024 * 1024 * 1024)

#define                     BIT(N)          ((QWORD)1 << N)
#define                     BYTE_MAX        (0xFF)
#define                     WORD_MAX        (0xFFFF)
#define                     DWORD_MAX       (0xFFFFFFFF)
#define                     QWORD_LOW(X)    ((QWORD)(X) & DWORD_MAX)
#define                     QWORD_HIGH(X)   ((QWORD)(X) >> 32)
#define                     DWORD_HIGH(X)   ((DWORD)(X) >> 16)
#define                     DWORD_LOW(X)    ((DWORD)(X) & WORD_MAX)
#define                     WORD_HIGH(X)    ((WORD)(X) >> 8)
#define                     WORD_LOW(X)     ((WORD)(X) & BYTE_MAX)

#define                     PAGE_SIZE       (4 * KILO)
#define                     PAGE_MASK       (-1 - 0xFFF)

/* Yields X rounded up to the nearest multiple of STEP.
For X >= 0, STEP >= 1 only. */
#define ROUND_UP(X, STEP) (((X) + (STEP) - 1) / (STEP) * (STEP))

/* Yields X divided by STEP, rounded up.
For X >= 0, STEP >= 1 only. */
#define DIV_ROUND_UP(X, STEP) (((X) + (STEP) - 1) / (STEP))

/* Yields X rounded down to the nearest multiple of STEP.
For X >= 0, STEP >= 1 only. */
#define ROUND_DOWN(X, STEP) ((X) / (STEP) * (STEP))

#define BITFIELD(HighBitPos, LowBitPos)             ((HighBitPos) - (LowBitPos) + 1)

#define ABS(a)                                      (((a) < 0) ? (-a) : (a))

//
// exported functions from __init.asm
//
//void __enableSSE(void); ???????

#endif // _MAIN_H_