%ifndef _DEFS_YASM_
%define _DEFS_YASM_
;;
;; Useful macros
;;

%idefine BIT(x)             (1 << x)

; write raw data to the VGA memory buffer as a basic output mechanism
%idefine PING(x, y, value)  mov     [0xB8000 + 2*(80*(y) + (x))], dword (value)


;;
;; Generic useful constants
;;
KILO                        equ 1024
MEGA                        equ 1024*1024
GIGA                        equ 1024*1024*1024
TERA                        equ 1024*1024*1024*1024
PAGE_SIZE                   equ 4096

%define PAGE_MASK               (-1 -0xFFF) ; this way (as a macro) we get the correct value from both 32 and 64 bits code
%define CR0_PE_DISABLE          0xFFFFFFFE
%define CR0_PE_ENABLE           0x00000001
%define HV_STACK_SIZE           1 * MEGA
%define STACK_MAGIC             0x12345678
%define ALIGN_STACK_32          0xFFFFFFE0
%define ALIGN_STACK_64          0xFFFFFFFFFFFFFFC0

;;
;; Define the main (hardcoded) characteristics of our runtime
;;
REAL_MODE_BASE              equ 0x8000                          ; PA where our 16 bit real mode code will execute
PE_HEADERS_SIZE             equ KILO                            ; offset to the first section (the PE headers could be parsed instead)

IMAGE_BASE_VA64             equ 0x80000000000                   ; our final 1T Virtual Address (VA) ImageBase (taken from the PE headers)
                                                                ; !ATENTIE! Trebuie tinut in sync cu KERNEL_MODE_BASE_VIRTUAL_MEM.

IMAGE_BASE_PA               equ 1 * MEGA                        ; our Physical Address (PA) where the PXE is asked to load the whole PE file
                                                                ; !ATENTIE! sa fie tinut sincronizat cu INITIAL_IMAGE_BASE_PA din codul C.

IMAGE_LENGTH                equ 8 * MEGA                        ; the maximum allowed/supported size for the PE file (can be changed)

BITMAPS_POOL_LENGTH         equ 2 * MEGA                        ;!!!!!keep in sync with BITMAPS_POOL_SIZE from memory_manager.c

PROTECTED_MODE_TOP_OF_STACK equ IMAGE_BASE_PA                   ; stack grow from big address to small, so it will dont bother us
REAL_MODE_TOP_OF_STACK      equ 0x7c00                          ; see the real-mode memory map on osdev: [0x500, 0x7c00) is a good&free memory range
INITIAL_BPS_STACK_SIZE      equ MEGA

;;
;; Define macros that allow us to compute the actual PA and VA associated with a label, with no need for PE relocations
;;
%define PA(label)           (IMAGE_BASE_PA + PE_HEADERS_SIZE + (label - $$))    ; the <4GB memory space occupied by the PE file
%define VA32(label)         (PA(label))                                         ; initial no-paging address space
%define VA64(label)         (IMAGE_BASE_VA64 + PE_HEADERS_SIZE + (label - $$))  ; final mapping at IMAGE_BASE_VA64
%define REBASE(label)       (REAL_MODE_BASE + (label - reloc))                     ; help for mapping in real-mode

%define SMAP_CODE                   0x0534D4150
%define SMAP_ENTRY_SIZE             24
%define SMAP_ENTRY_SIZE_COMMONLY    20

FLAT_DESCRIPTOR_CODE64      equ 0x002F9A000000FFFF                  ; Code: Execute/Read
FLAT_DESCRIPTOR_DATA64      equ 0x00CF92000000FFFF                  ; Data: Read/Write
FLAT_DESCRIPTOR_CODE32      equ 0x00CF9A000000FFFF                  ; Code: Execute/Read
FLAT_DESCRIPTOR_DATA32      equ 0x00CF92000000FFFF                  ; Data: Read/Write
FLAT_DESCRIPTOR_CODE16      equ 0x00009B000000FFFF                  ; Code: Execute/Read, accessed
FLAT_DESCRIPTOR_DATA16      equ 0x000093000000FFFF                  ; Data: Read/Write, accessed

IA32_EFER                   equ 0xC0000080
CR4_PAE                     equ 0x00000020
IA23_EFER_LME               equ 0x100
CR0_PG                      equ bit(31)

; paging access and rights for VA to PA translations
VA_PRESENT                  equ bit(0)
VA_WRITE_ACCESS             equ bit(1)
VA_USER_ACCESS              equ bit(2)
VA_WRITETHROUGH             equ bit(3)
VA_CACHE_DISABLE            equ bit(4)
VA_ACCESSED                 equ bit(5)
VA_DIRTY                    equ bit(6)
VA_PAGE_SIZE                equ bit(7)
VA_GLOBAL                   equ bit(8)
VA_MASK                     equ 0x1FF


;;
;; MULTIBOOT constants
;;
MULTIBOOT_HEADER_SIZE       equ 48                                  ; check out '3.1.1 The layout of Multiboot header'
MULTIBOOT_HEADER_MAGIC      equ 0x1BADB002
MULTIBOOT_HEADER_FLAGS      equ MB_HEADERFLAG_PAGE_ALIGNED | MB_HEADERFLAG_MEM_MAP | MB_HEADERFLAG_FIXED_BASE
MULTIBOOT_LOADER_MAGIC      equ 0x2BADB002
MB_HEADERFLAG_PAGE_ALIGNED  equ bit(0)
MB_HEADERFLAG_MEM_MAP       equ bit(1)
MB_HEADERFLAG_FIXED_BASE    equ bit(16)



;;
;; MULTIBOOT data structures
;;

struc MULTIBOOT_HEADER
    .magic                  resd 1
    .flags                  resd 1
    .checksum               resd 1
    .header_addr            resd 1
    .load_addr              resd 1
    .load_end_addr          resd 1
    .bss_end_addr           resd 1
    .entry_addr             resd 1
    .mode_type              resd 1
    .width                  resd 1
    .height                 resd 1
    .depth                  resd 1
endstruc


struc MULTIBOOT_INFO
    flags                   resd 1              ; 0       | flags             |    (required)
                                                ; +-------------------+
    mem_lower               resd 1              ; 4       | mem_lower         |    (present if flags[0] is set)
    mem_upper               resd 1              ; 8       | mem_upper         |    (present if flags[0] is set)
                                                ; +-------------------+
    boot_device             resd 1              ; 12      | boot_device       |    (present if flags[1] is set)
                                                ; +-------------------+
    cmdline                 resd 1              ; 16      | cmdline           |    (present if flags[2] is set)
                                                ; +-------------------+
    mods_count              resd 1              ; 20      | mods_count        |    (present if flags[3] is set)
    mods_addr               resd 1              ; 24      | mods_addr         |    (present if flags[3] is set)
                                                ; +-------------------+
    syms                    resb 44-28          ; 28 - 40 | syms              |    (present if flags[4] or
                                                ; |                   |                flags[5] is set)
                                                ; +-------------------+
    mmap_length             resd 1              ; 44      | mmap_length       |    (present if flags[6] is set)
    mmap_addr               resd 1              ; 48      | mmap_addr         |    (present if flags[6] is set)
                                                ; +-------------------+
    drives_length           resd 1              ; 52      | drives_length     |    (present if flags[7] is set)
    drives_addr             resd 1              ; 56      | drives_addr       |    (present if flags[7] is set)
                                                ; +-------------------+
    config_table            resd 1              ; 60      | config_table      |    (present if flags[8] is set)
                                                ; +-------------------+
    boot_loader_name        resd 1              ; 64      | boot_loader_name  |    (present if flags[9] is set)
                                                ; +-------------------+
    apm_table               resd 1              ; 68      | apm_table         |    (present if flags[10] is set)
                                                ; +-------------------+
    vbe_control_info        resd 1              ; 72      | vbe_control_info  |    (present if flags[11] is set)
    vbe_mode_info           resd 1              ; 76      | vbe_mode_info     |
    vbe_mode                resd 1              ; 80      | vbe_mode          |
    vbe_interface_seg       resd 1              ; 82      | vbe_interface_seg |
    vbe_interface_off       resd 1              ; 84      | vbe_interface_off |
    vbe_interface_len       resd 1              ; 86      | vbe_interface_len |
                                                ; +-------------------+
endstruc

%endif