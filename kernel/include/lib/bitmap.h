#include "status.h"

typedef enum _BIT_STATUS
{
    BIT_STATUS_FREE    = 0,
    BIT_STATUS_USED    = 1,
}BIT_STATUS;


/* Element type.

This must be an unsigned integer type at least as wide as int.

Each bit represents one bit in the bitmap.
If bit 0 in an element represents bit K in the bitmap,
then bit 1 in the element represents bit K+1 in the bitmap,
and so on. */
typedef QWORD BitmapElemType;

typedef struct _BITMAP
{
    BitmapElemType* Bitmap;
    QWORD           NumberOfBits;
}BITMAP;

/*****************************/
/* Early init (if no heap), Creation and destruction */
// Return NULL if fail
void            BitmapEarlyInit(BITMAP* Bitmap, QWORD BaseAddress, QWORD NumberOfBits);
BITMAP*         BitmapCreate(QWORD NumberOfBits);
STATUS          BitmapDestroy(BITMAP* Bitmap);

/************************************/
/* Setting and testing single bits. */
STATUS          BitmapSet(BITMAP* Bitmap, QWORD Index, BIT_STATUS Value);
BOOLEAN         BitmapTest(BITMAP* Bitmap, QWORD Index);

/**************************************/
/* Setting and testing multiple bits. */
STATUS          BitmapSetAll(BITMAP* Bitmap, BIT_STATUS Value);
STATUS          BitmapSetMultiple(BITMAP* Bitmap, QWORD Start, QWORD Cnt, BIT_STATUS Value);
// Finds and returns in Index parameter the starting index of the first group of CNT
// consecutive bits in B at or after START that are all set to VALUE.
STATUS          BitmapScanMultiple(BITMAP* Bitmap, QWORD Cnt, BIT_STATUS Value, QWORD* Index);