#ifndef _MSR_H_
#define _MSR_H_

#include "lib/std.h"


#pragma warning(disable:4214)   // disable bit field types other than int warning
#pragma warning(disable:4201)   // disable nameless struct warning

#define IA32_PAT    0x00000277
typedef union _IA32_PAT_REGISTER
{
    struct
    {
        QWORD Pa0           : 3;
        QWORD Reserved1     : 5;
        QWORD Pa1           : 3;
        QWORD Reserved2     : 5;
        QWORD Pa2           : 3;
        QWORD Reserved3     : 5;
        QWORD Pa3           : 3;
        QWORD Reserved4     : 5;
        QWORD Pa4           : 3;
        QWORD Reserved5     : 5;
        QWORD Pa5           : 3;
        QWORD Reserved6     : 5;
        QWORD Pa6           : 3;
        QWORD Reserved7     : 5;
        QWORD Pa7           : 3;
    };

    QWORD Raw;
} IA32_PAT_REGISTER;
static_assert(sizeof(IA32_PAT_REGISTER) == sizeof(QWORD), "Invalid IA32_PAT_REGISTER size.");

#define IA32_EFER   0xC0000080
typedef union _IA32_EFER_REGISTER
{
    struct
    {
        QWORD SyscallEnable             : BITFIELD(0, 0);
        QWORD Reserved1                 : BITFIELD(7, 1);
        QWORD Ia32eModeEnable           : BITFIELD(8, 8);
        QWORD Reserved2                 : BITFIELD(9, 9);
        QWORD Ia32eModeActive           : BITFIELD(10, 10);
        QWORD ExecuteDisableBitEnable   : BITFIELD(11, 11);
    };

    QWORD Raw;
} IA32_EFER_REGISTER;
static_assert(sizeof(IA32_EFER_REGISTER) == sizeof(QWORD), "Invalid IA32_EFER_REGISTER size.");

#pragma warning(default:4201)   // enable nameless struct warning
#pragma warning(default:4214)   // enable bit field types other than int warning

#endif // !_MSR_H_
