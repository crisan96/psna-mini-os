#ifndef _IDT_H_
#define _IDT_H_

#include "lib/status.h"

/* There are 256 interrupts (0..255), so IDT should have 256 entries,
each entry corresponding to a specific interrupt. There should therefore be
at least enough entries so a GPF can be caught.*/
#define IDT_MAX_ENTRIES                 256
#define USER_EXCEPTIONS_FIRST_VECTOR    32

typedef struct _INTERRUPT_REGISTER_STUCTURE INTERRUPT_REGISTER_STUCTURE;
typedef struct _COMMON_REGISTER_STRUCTURE COMMON_REGISTER_STRUCTURE;

/* Functions */
/*1*/ void      IdtInitialize(void);
/*2*/ STATUS    IdtLoadForCurrentProcessor(void);

#endif // !_IDT_H_
