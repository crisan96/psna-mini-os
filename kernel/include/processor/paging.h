#ifndef _PAGING_H_
#define _PAGING_H_

#include "lib/status.h"
#include "processor/cpu.h"

typedef enum _ACCESS_RIGHT
{
    //ACCESS_RIGHT_READ   = BIT(0), // Pentru kernel mode read-ul este implicit. Inca nu suportam User-Mode
    ACCESS_RIGHT_WRITE  = BIT(1),
    ACCES_RIGHT_EXECUTE = BIT(2)
}ACCESS_RIGHT;

/*1*/ STATUS    PagingInit(void);

// This function will init given Cr3 such that
// starting from 8 TERA we will map kernel memory and nothing in rest
/*2*/ STATUS    PagingMapKernelMemory(QWORD* Cr3);

/*3*/ STATUS    PagingMapVaToPa(QWORD* Cr3, BOOLEAN CreateNewCr3, QWORD Va, QWORD Pa, ACCESS_RIGHT AccessRights);

/*4*/ STATUS    PagingMapVaToPaQuick(QWORD Va, QWORD Pa, ACCESS_RIGHT AccessRights);

/*5*/ STATUS    PagingMapVaToPaRange(QWORD* Cr3, BOOLEAN CreateNewCr3, QWORD StartVa, QWORD StartPa, QWORD NumberOfPages, ACCESS_RIGHT AccessRights);

/*6*/ STATUS    PagingMapVaToPaRangeQuick(QWORD StartVa, QWORD StartPa, QWORD NumberOfPages, ACCESS_RIGHT AccessRights);

// If IsUserModeCr3 is set then it should get physical meomry
// for paging structures from the dedicated page pool for that process
/*7*/ STATUS    PagingMapVaToPaEx(QWORD* Cr3, BOOLEAN CreateNewCr3, QWORD Va, QWORD Pa, CPU_MEMORY_TYPE MemoryType, ACCESS_RIGHT AccessRights);

/*8*/ STATUS    PagingUnmapAddress(QWORD Va);

/*9*/ STATUS    PagingUnmapAddressRange(QWORD Va, QWORD NumberOfPages);

/*10*/STATUS    PagingUnmapAddressEx(QWORD Cr3, QWORD Va);
#endif // !_PAGING_H_
