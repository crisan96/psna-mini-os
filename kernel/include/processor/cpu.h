#ifndef _CPU_H_
#define _CPU_H_

#define MAXPHYADDR                              (52)

typedef enum _CPU_MEMORY_TYPE
{
    CPU_MEMORY_TYPE_STRONG_UNCACHEABLE          = 0,
    CPU_MEMORY_TYPE_WRITECOMBINE                = 1,
    CPU_MEMORY_TYPE_WRITETHROUGH                = 4,
    CPU_MEMORY_TYPE_WRITEPROTECT                = 5,
    CPU_MEMORY_TYPE_WRITEBACK                   = 6,
    CPU_MEMORY_TYPE_UNCACHEABLE                 = 7,
}CPU_MEMORY_TYPE;

#include "lib/status.h"

/* Functions */
/*1*/ STATUS    CpuEnableSSE(void);

// This function set registers as we want for current CPU
// For example, clear CR0.CD to enable caching and so on..
// This function must be called by every CPU
/*2*/ STATUS    CpuSetRegistersForCurrentCpu(void);

/*3*/ void      CpuReset(void);

#endif // !_CPU_H_
