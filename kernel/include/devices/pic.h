#ifndef _PIC_H_
#define _PIC_H_

#include "lib/status.h"

#define PIC1_BASE_VECTOR 0x28
#define PIC2_BASE_VECTOR 0x30

/*1*/ STATUS    PicInit(BYTE BaseVectorPic1, BYTE BaseVectorPic2);
/*2*/ STATUS    PicMaskIrqLine(BYTE Irq);
/*3*/ STATUS    PicUnmaskIrqLine(BYTE Irq);
/*5*/ STATUS    PicSendEoi(BYTE Irq);

// This function return BYTE_MAX (= 0xFF) if InterruptVector is bad.
// 0xFF can be use as an error signal because a normal return value should be
// in [0,15] interval
/*6*/ BYTE      PicGetIrqFromInterruptVector(BYTE InterruptVector);

#endif // !_PIC_H_
