#ifndef _MMAP_H_
#define _MMAP_H_

#include "lib/std.h"

typedef enum _MEMORY_TYPE
{
    MEMORY_TYPE_USBALE          = 1,
    MEMORY_TYPE_RESERVED,
    MEMORY_TYPE_ACPI_MEMORY,
    MEMORY_TYPE_ACPI_NVS_MEMORY,
    MEMORY_TYPE_BAD_MEMORY,
}MEMORY_TYPE;

#pragma pack(push, 1)
typedef struct _MMAP_ENTRY
{
    QWORD BaseAddress;          // Base address of this hunk of ram
    QWORD Length;               // Length of "region" (if this value is 0, ignore the entry)
    DWORD Type;                 // Region type: 1 -> Usable(normal) RAM, 2 -> Reserved(unusable), 3 -> ACPI reclaimed memory, 4 -> ACPI NVS memory, 5 -> Area containing bad memory
    DWORD ExtendedAttributes;
}MMAP_ENTRY;
#pragma pack(pop)

/*1*/ void MmapDump(void);

#endif // !_MMAP_H_
