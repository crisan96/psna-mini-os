#ifndef _MEMORY_MANAGER_H_
#define _MEMORY_MANAGER_H_

#include "lib/status.h"

/*
    PAT este un mecanism de management al memoriei pt procesor. Tipurile de memorie
    pt procesoarele intel sunt enumerate in cpu.h. Noi vom folosi paginarea pt determinarea
    memoriei: mai precis, o combinatie intre MTRR-uri (ce ar trebui sa fie setate de BIOS) si
    PAT (care la fel ar trebui sa fie setat cum e mai jos, daca nu il setam noi).
    Noi va trebui in structurile de paginare sa folosim index-uri din PAT pentru a specifica
    ce memorie va referi o structura de paginare (deocamdata noi vom folosi WB si eventual UC)
*/
#define     EXPECTED_IA32_PAT_VALUES                (0x0007'0406'0007'0406ULL)
#define     UNCACHEABLE_MEOMRY_INDEX_IN_PAT         (3)
#define     WRITE_BACK_MEMORY_TYPE_INDEX_IN_PAT     (0)

/*1*/ STATUS    MemManagerInit(void);

/*2*/ STATUS    MemManagerPaToVa(QWORD Pa, QWORD* Va);

/*3*/ STATUS    MemManagerAllocPageForPagingStructure(QWORD* Pa);

/*4*/ void*     MemManagerAlloc(QWORD Size);

/*5*/ void      MemManagerFree(void* Address);

#endif // !_MEMORY_MANAGER_H_
