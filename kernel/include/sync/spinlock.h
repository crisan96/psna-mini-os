#ifndef _SPINLOCK_H_
#define _SPINLOCK_H_

#include "lib/status.h"

// Deocamdata avem nevoie sa expunem structura in .h
// Cred ca normal ar fi frumos sa nu o expunem dar asta ar insemna
// ca in exteriorul lui spinlock.c lucram numa cu pointer la structura
// dar cum nu avem alocator de memorie trebuie sa trimitem aici structura
// deja alocata (pe stiva, sau in segment de date globale)
typedef struct _SPINLOCK
{
    volatile DWORD Lock;
}SPINLOCK;

/*1*/ STATUS SpinlockInit(SPINLOCK* Spinlock);
/*2*/ STATUS SpinlockAquire(SPINLOCK* Spinlock);
/*3*/ STATUS SpinlockRelease(SPINLOCK* Spinlock);

#endif
