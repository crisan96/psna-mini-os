#ifndef _VGA_H_
#define _VGA_H_

#include "lib/status.h"

/* Video memory page size */
#define VGA_HEIGHT              25ULL
#define VGA_WIDTH               80ULL

typedef enum _VGA_STANDARD_COLOR
{
    VGA_COLOR_BLACK       ,
    VGA_COLOR_BLUE        ,
    VGA_COLOR_GREEN       ,
    VGA_COLOR_CYAN        ,
    VGA_COLOR_RED         ,
    VGA_COLOR_PURPLE      ,
    VGA_COLOR_BROWN       ,
    VGA_COLOR_GRAY        ,
    VGA_COLOR_DARK_GRAY   ,
    VGA_COLOR_LIGHT_BLUE  ,
    VGA_COLOR_LIGHT_GREEN ,
    VGA_COLOR_LIGHT_CYAN  ,
    VGA_COLOR_LIGHT_RED   ,
    VGA_COLOR_LIGHT_PURPLE,
    VGA_COLOR_YELLOW      ,
    VGA_COLOR_WHITE       ,
    VGA_COLOR_END,
}VGA_STANDARD_COLOR;

/*1*/ STATUS                VgaInit(void);

/*2*/ STATUS                VgaClear(VGA_STANDARD_COLOR BackgroundColor);

/*3*/ STATUS                VgaPrintChar(BYTE CoordX, BYTE CoordY, CHAR Character, VGA_STANDARD_COLOR BackgroundColor, VGA_STANDARD_COLOR ForegroundColor);

/*4*/ STATUS                VgaPrintString(BYTE StartCoordX, BYTE StartCoordY, CHAR* String, DWORD StringSize, VGA_STANDARD_COLOR BackgroundColor, VGA_STANDARD_COLOR ForegroundColor);

/*5*/ STATUS                VgaSetBackgroundColor(VGA_STANDARD_COLOR BackgroundColor);

/*6*/ VGA_STANDARD_COLOR    VgaGetBackgroundColor(void);

#endif // !_VGA_H_
