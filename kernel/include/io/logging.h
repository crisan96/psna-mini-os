#ifndef _LOGGING_H_
#define _LOGGING_H_

#include "lib/crt.h"
#include "io/serial.h"
#include "global/global.h"

#define LOG(buf, ...)                   SpinlockAquire(&(KernelGlobalData.Logging.Spinlock));                               \
                                        crt_sprintf(KernelGlobalData.Logging.Output, "%s : %d :", __FILE__, __LINE__);      \
                                        SerialWriteBuffer(KernelGlobalData.Logging.Output);                                 \
                                        crt_sprintf(KernelGlobalData.Logging.Output, buf, __VA_ARGS__);                     \
                                        SerialWriteBuffer(KernelGlobalData.Logging.Output);                                 \
                                        SpinlockRelease(&(KernelGlobalData.Logging.Spinlock));                         \

#define LOG_INFO(buf, ...)              LOG("[INFO]"##buf, __VA_ARGS__)
#define LOG_WARNING(buf, ...)           LOG("[WARNING]"##buf, __VA_ARGS__)
#define LOG_ERROR(buf, ...)             LOG("[ERROR]"##buf, __VA_ARGS__)

#endif // !_LOGGING_H_