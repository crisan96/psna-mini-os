#ifndef _SERIAL_H_
#define _SERIAL_H_

#include "lib/status.h"

/*1*/ STATUS SerialInit(void);
/*2*/ STATUS SerialWriteByte(BYTE Data);
/*3*/ STATUS SerialWriteBuffer(CHAR* Buffer);
/*4*/ STATUS SerialWriteNBuffer(CHAR* Buffer, DWORD Length);

#endif // !_SERIAL_H_