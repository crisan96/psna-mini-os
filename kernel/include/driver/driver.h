#ifndef _DRIVER_H_
#define _DRIVER_H_

#include "lib/status.h"
#include "processor/idt.h"
#include "devices/pic.h"

/* Lista vectorului de intreruperi din IDT corespunzator fiecarui driver */
#define TIMER_DRIVER_INTERRUPT_VECTOR        (PIC1_BASE_VECTOR)
#define KEYBOARD_DRIVER_INTERRUPT_VECTOR     (TIMER_DRIVER_INTERRUPT_VECTOR + 1)

/*
    Cum arata callback-ul pentru driver entry-ul unui driver
    si pentru dezactivarea unui driver (in caz ca are probleme)
*/
typedef STATUS  (*InterruptServiceRoutine)(COMMON_REGISTER_STRUCTURE* CommonRegisters, INTERRUPT_REGISTER_STUCTURE* InterruptRegisters);
typedef void    (*DriverDisableCallback)(void);

/*1*/ STATUS    DriverInit(void);
/*2*/ STATUS    DriverRegister(BYTE InterruptVector, InterruptServiceRoutine Isr, DriverDisableCallback DriverDisableFunction);
/*3*/ STATUS    DriverUnregister(BYTE InterruptVector);
/*4*/ STATUS    DriverCall(BYTE InterruptVector, COMMON_REGISTER_STRUCTURE* CommonRegisters, INTERRUPT_REGISTER_STUCTURE* InterruptRegisters);
/*5*/ STATUS    DriverDisable(BYTE InterruptVector);

#endif // !_DRIVER_H_
