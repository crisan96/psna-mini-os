#ifndef _TIMER_H_
#define _TIMER_H_

#include "lib/status.h"

typedef void (*TimerCallbackFunction)(void);

/*1*/ STATUS    TimerDriverInit(void);

// After init, keyboard driver is enabled by default
/*2*/ STATUS    TimerdDriverEnable(void);

/*3*/ void      TimerdDriverDisable(void);

// Every component can register callbacks to timer driver.
// When timer is triggered, the driver do its job (increment system ticks),
// and then call callbacks registered. If TriggerAtEveryXTicks is TRUE, then the callback
// is call just if global ticks is multiple of X.
/*4*/ STATUS    TimerDriverRegisterCallback(TimerCallbackFunction Callback, BOOLEAN TriggerAtEveryXTicks, QWORD X);

/*5*/ STATUS    TimerDriverUnregisterCallback(TimerCallbackFunction Callback);

/*7*/ QWORD     GetSystemTIcks(void);

#endif // !_TIMER_H_
