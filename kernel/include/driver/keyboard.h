#ifndef _KEYBOARD_H_
#define _KEYBOARD_H_

#include "lib/status.h"

typedef enum _KEY_CODE
{
    KEY_ENTER,
    KEY_BACKSPACE,
    KEY_ESCAPE,
    KEY_UNKNOWN
}KEY_CODE;

typedef void (*KeyboardCallbackFunction)(void);

/*1*/ STATUS KeyboardDriverInit(void);

// After init, keyboard driver is enabled by default
/*2*/ STATUS    KeyboardDriverEnable(void);

/*3*/ void      KeyboardDriverDisable(void);

// Every component can register callbacks to keyboard driver.
// When keyboard is triggered, the driver do its job (save the last key pressed, etc),
// and then call callbacks registered. If TriggerAtSpecificKey is TRUE, then the callback
// is call just if last pressed key is equal to Key
/*4*/ STATUS    KeyboardDriverRegisterCallback(KeyboardCallbackFunction Callback, BOOLEAN TriggerAtSpecificKey, CHAR Key);

/*5*/ STATUS    KeyboardDriverUnregisterCallback(KeyboardCallbackFunction Callback);

/*6*/ CHAR      KeyboardDriverGetLastKeyPressed(void);
#endif // !_KEYBOARD_H_
