#ifndef _GLOBAL_H_
#define _GLOBAL_H_

#include "lib/status.h"
#include "sync/spinlock.h"
#include "memory/mmap.h"

#define ASM_DATA_MEM_ALLOCATED       (0x8 + 0x4 + 0x1B00) // Keep in sync with struc ASM_DATA size

#pragma pack(push, 1)
#pragma warning(disable:4201)   // disable nameless struct warning
typedef union _ASM_DATA
{
    struct
    {
        QWORD   PagingStructureNextEntry;   // In cadrul codului assembly, am pregatit si acolo deja
                                            // un pic din pool-urile de memorie. Mai cu seama pool-ul
                                            // de paginare. Aceasta valoare este adresa FIZICA a ultimei
                                            // pagini de memorie ce a fost folosita din page pool. (paginarea
                                            // a inceput inca din assembly).

        struct
        {
            DWORD       NumberOfEntries;
            MMAP_ENTRY  MmapEntry[1];
        }MemoryMap;
    };

    BYTE    Raw[ASM_DATA_MEM_ALLOCATED];
}ASM_DATA;
#pragma warning(default:4201)   // enable nameless struct warning
#pragma pack(pop)
static_assert(sizeof(ASM_DATA) == ASM_DATA_MEM_ALLOCATED, "Invalid ASM_DATA size.");

// TODO: Pe viitor sa mai taiam din ce este aici.
// Sa incercam sa tinem global cat se poate de putin.
// De exemplu, sa se ocupe memory_manager-ul de memory map-ul din assembly si sa-l tina privat acolo
typedef struct _KERNEL_GLOBAL_DATA
{
    struct
    {
#define     LOG_BUFFER_SIZE     512
        SPINLOCK    Spinlock;   // avoid inconsistent output when two processor write to serial port at the same time
        CHAR        Output[LOG_BUFFER_SIZE];
    }Logging;

    ASM_DATA* AsmData;
}KERNEL_GLOBAL_DATA;
extern KERNEL_GLOBAL_DATA KernelGlobalData;

STATUS KernelGlobalDataInit(ASM_DATA* AsmData);

#endif // !_GLOBAL_H_