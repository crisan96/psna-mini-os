#include "devices/pic.h"
#include "lib/intrin.h"
#include "io/logging.h"
#include "processor/idt.h"

//  PCI1 and PCI2 I/O Ports Base
#define PIC1            0x20
#define PIC2            0xA0

//  PCI1 and PCI2 Control Registers
#define PIC1_COMMAND    0x20
#define PIC1_DATA       0x21
#define PIC2_COMMAND    0xA0
#define PIC2_DATA       0xA1

//  Eoi value
#define PIC_EOI         0x20

//  Configuration values
#define ICW3_MASK 0x7
#define ICW1_INIT                   0x10        // Initiate configuration
#define ICW1_ICW4                   0x01        // Determines the configuration will be MASTER SLAVE next ICW MASTER SLAVE next ICW
#define ICW3_MASTER_IRQ2            0x4         // Communicate with slave PIC via IRQ2
#define ICW3_SLAVE_CASCADE          0x2         // The master will cascade-communicate via IRQ2
#define ICW4_INTEL_ARCHITECTURE	    0x01        // Bit 0 must be set in order to indicate the controoler are operating in Intel Architecture

static BYTE gBaseVectorPic1;
static BYTE gBaseVectorPic2;

static __forceinline BYTE _GetInterruptVectorFromIrq(BYTE Irq);

/*1*/
STATUS
PicInit(
    BYTE BaseVectorPic1,
    BYTE BaseVectorPic2
)
{
    if (BaseVectorPic1 >= BaseVectorPic2) { return STATUS_INVALID_PARAMETERS; }

    gBaseVectorPic1 = BaseVectorPic1;
    gBaseVectorPic2 = BaseVectorPic2;

    BYTE pic1InterruptMask = __inbyte(PIC1_DATA);
    BYTE pic2InterruptMask = __inbyte(PIC2_DATA);

    // Send ICW1 in COMMAND register
    __outbyte(PIC1_COMMAND, (ICW1_INIT | ICW1_ICW4));
    _io_wait();
    __outbyte(PIC2_COMMAND, (ICW1_INIT | ICW1_ICW4));
    _io_wait();

    // Send ICW2 in DATA register
    LOG_INFO("[%s] Base vecor for Pic1 : 0x%x, base vector for Pic2 : 0x%x\n", __func__, BaseVectorPic1, BaseVectorPic2);
    __outbyte(PIC1_DATA, BaseVectorPic1);
    _io_wait();
    __outbyte(PIC2_DATA, BaseVectorPic2);
    _io_wait();

    // Send ICW3 in DATA register
    __outbyte(PIC1_DATA, ICW3_MASTER_IRQ2);
    _io_wait();
    __outbyte(PIC2_DATA, ICW3_SLAVE_CASCADE);
    _io_wait();

    // Send ICW4 in DATA register
    __outbyte(PIC1_DATA, ICW4_INTEL_ARCHITECTURE);
    _io_wait();
    __outbyte(PIC2_DATA, ICW4_INTEL_ARCHITECTURE);
    _io_wait();

    // Reload masks
    __outbyte(PIC1_DATA, pic1InterruptMask);
    __outbyte(PIC2_DATA, pic2InterruptMask);

    return STATUS_SUCCESS;
}

/*2*/
STATUS
PicMaskIrqLine(
    BYTE Irq
)
{
    WORD port;
    BYTE value;

    if (Irq > 15) { return STATUS_INVALID_PARAMETER_1; }

    if(Irq < 8)
    {
        port = PIC1_DATA;
    }
    else
    {
        port = PIC2_DATA;
        Irq -= 8;
    }

    value = __inbyte(port) | BIT(Irq);
    __outbyte(port, value);

    return STATUS_SUCCESS;
}

/*3*/
STATUS
PicUnmaskIrqLine(
    BYTE Irq
)
{
    WORD port;
    BYTE value;

    if (Irq > 15) { return STATUS_INVALID_PARAMETER_1; }

    if(Irq < 8)
    {
        port = PIC1_DATA;
    }
    else
    {
        port = PIC2_DATA;
        Irq -= 8;
    }

    value = __inbyte(port) & ~BIT(Irq);
    __outbyte(port, value);

    return STATUS_SUCCESS;
}

/*5*/
STATUS
PicSendEoi(
    BYTE Irq
)
{
    if (Irq > 15) { return STATUS_INVALID_PARAMETER_1; }

    if (Irq < 8)
    {
        __outbyte(PIC1_COMMAND, PIC_EOI);
    }
    else
    {
        __outbyte(PIC1_COMMAND, PIC_EOI);
        __outbyte(PIC2_COMMAND, PIC_EOI);
    }

    return STATUS_SUCCESS;
}

/*6*/
BYTE
PicGetIrqFromInterruptVector(
    BYTE InterruptVector
)
{
    if ((InterruptVector >= gBaseVectorPic1)
        && (InterruptVector < gBaseVectorPic1 + 8)
        )
    {
        return InterruptVector - gBaseVectorPic1;
    }
    else if ((InterruptVector >= gBaseVectorPic2)
        && (InterruptVector < gBaseVectorPic2 + 8)
        )
    {
        return InterruptVector - gBaseVectorPic2 + 8;
    }
    else
    {
        return BYTE_MAX;
    }
}

static
__forceinline
BYTE
_GetInterruptVectorFromIrq(
    BYTE Irq
)
{
    // Irq is sent from the slave PIC
    if(Irq > 7)
    {
        return (Irq - 8) + gBaseVectorPic2;
    }
    else
    {
        return  Irq + gBaseVectorPic1;
    }
}