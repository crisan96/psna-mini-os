#include "driver/driver.h"
#include "processor/idt.h"
#include "lib/crt.h"

typedef struct _ISR_INFO
{
    BOOLEAN                 IsAvailable;
    InterruptServiceRoutine Isr;
    DriverDisableCallback   DriverDisableFunction;
}ISR_INFO;

/*
    Structura statica globala pentru modulul de drivere unde vom tine minte
    driver entry-urile.
    De ex, driver-ul pentru PIT timer este declansat de intreruperea 32, deci
    pe pozitia 32 vom avea driver entry-ul timerului.
*/
static ISR_INFO gDrivers[IDT_MAX_ENTRIES - USER_EXCEPTIONS_FIRST_VECTOR];

/* Interrupt vector 32 va avea driver index 0 si asa mai departe */
static __forceinline BYTE _ConvetInterruptVectorToDriverIndex(BYTE InterruptVector);

/*1*/
STATUS
DriverInit(
    void
)
{
    crt_memset((void*)&gDrivers, 0, sizeof(gDrivers));

    return STATUS_SUCCESS;
}

/*1*/
STATUS
DriverRegister(
    BYTE                    InterruptVector,
    InterruptServiceRoutine Isr,
    DriverDisableCallback   DriverDisableFunction
)
{
    if ((InterruptVector < USER_EXCEPTIONS_FIRST_VECTOR)
        || (InterruptVector >= IDT_MAX_ENTRIES))
        {return STATUS_INVALID_PARAMETER_1;}

    if (!Isr) { return STATUS_INVALID_PARAMETER_2; }

    if (!DriverDisableFunction) { return STATUS_INVALID_PARAMETER_3; }

    BYTE driverIndex = _ConvetInterruptVectorToDriverIndex(InterruptVector);

    if (gDrivers[driverIndex].IsAvailable) { return STATUS_DRIVER_ALREADY_EXIST; }

    gDrivers[driverIndex].Isr = Isr;
    gDrivers[driverIndex].DriverDisableFunction = DriverDisableFunction;
    gDrivers[driverIndex].IsAvailable = TRUE;

    return STATUS_SUCCESS;
}

/*2*/
STATUS
DriverUnregister(
    BYTE InterruptVector
)
{
    if ((InterruptVector < USER_EXCEPTIONS_FIRST_VECTOR)
        || (InterruptVector >= IDT_MAX_ENTRIES))
        return STATUS_INVALID_PARAMETER_1;

    BYTE driverIndex = _ConvetInterruptVectorToDriverIndex(InterruptVector);

    if (!gDrivers[driverIndex].IsAvailable) return STATUS_DRIVER_ALREADY_UNREGISTERED;

    gDrivers[driverIndex].Isr = NULL;
    gDrivers[driverIndex].IsAvailable = FALSE;

    return STATUS_SUCCESS;
}

/*3*/
STATUS
DriverCall(
    BYTE                        InterruptVector,
    COMMON_REGISTER_STRUCTURE   *CommonRegisters,
    INTERRUPT_REGISTER_STUCTURE *InterruptRegisters
)
{
    if ((InterruptVector < USER_EXCEPTIONS_FIRST_VECTOR)
        || (InterruptVector >= IDT_MAX_ENTRIES))
        return STATUS_INVALID_PARAMETER_1;

    if (!CommonRegisters) { return STATUS_INVALID_PARAMETER_2; }

    if (!InterruptRegisters) { return STATUS_INVALID_PARAMETER_3; }

    BYTE driverIndex = _ConvetInterruptVectorToDriverIndex(InterruptVector);

    if (gDrivers[driverIndex].IsAvailable)
    {
        return gDrivers[driverIndex].Isr(CommonRegisters, InterruptRegisters);
    }
    else
    {
        return STATUS_DRIVER_DO_NOT_EXIST;
    }
}

/*4*/
STATUS
DriverDisable(
    BYTE InterruptVector
)
{
    if ((InterruptVector < USER_EXCEPTIONS_FIRST_VECTOR)
        || (InterruptVector >= IDT_MAX_ENTRIES))
    {return STATUS_INVALID_PARAMETER_1;}

    BYTE driverIndex = _ConvetInterruptVectorToDriverIndex(InterruptVector);

    if (gDrivers[driverIndex].IsAvailable)
    {
        gDrivers[driverIndex].DriverDisableFunction();
        return STATUS_SUCCESS;
    }
    else
    {
        return STATUS_DRIVER_DO_NOT_EXIST;
    }
}

static
__forceinline
BYTE
_ConvetInterruptVectorToDriverIndex(
    BYTE InterruptVector
)
{
    return InterruptVector - USER_EXCEPTIONS_FIRST_VECTOR;
}