#include "driver/keyboard.h"
#include "driver/driver.h"
#include "io/logging.h"

#pragma warning(disable:4324)   // disable structure was padded due to alignment specifier warning
typedef struct _KEYBOARD_DRIVER_GLOBAL_DATA
{
    BOOLEAN                             IsInitialized;

    __declspec (align(8)) volatile char IsKeyboardDriverEnabled;

#define MAX_CALLBACKS   BYTE_MAX
    KeyboardCallbackFunction            Callbacks[MAX_CALLBACKS];

    CHAR                                LastKeyPressed;
    BYTE                                LastKeyPressedContor;   // am observat ca apare tasta si pentru "press" si pentru "release"
                                                                // ca sa nu o luam de doua ori punem un contor... (ideal ar fi citit mai bine
                                                                // cum functioneaza PS2... :D)
}KEYBOARD_DRIVER_GLOBAL_DATA;
#pragma warning(default:4324)   // enable structure was padded due to alignment specifier warning
static KEYBOARD_DRIVER_GLOBAL_DATA gKeyboardDriverGlobalData;

/* Driver function */
static STATUS _KeyboardDriver(COMMON_REGISTER_STRUCTURE* CommonRegisters, INTERRUPT_REGISTER_STUCTURE* InterruptRegisters);

/* Static functions */
static __forceinline CHAR       _DecodeKey(BYTE Command);
static __forceinline void       _KeyboardDriverEnable(void);
static __forceinline void       _KeyboardDriverDisable(BOOLEAN MaskDriverIrq);
static __forceinline BOOLEAN    _IsKeyboardDriverEnabled(void);

/*1*/
STATUS
KeyboardDriverInit(
    void
)
{
    crt_memset((void*)&gKeyboardDriverGlobalData, 0, sizeof(gKeyboardDriverGlobalData));

    STATUS status = DriverRegister(KEYBOARD_DRIVER_INTERRUPT_VECTOR, _KeyboardDriver, KeyboardDriverDisable);
    if (SUCCESS(status))
    {
        gKeyboardDriverGlobalData.IsInitialized = TRUE;
        _KeyboardDriverEnable();
    }
    else
    {
        gKeyboardDriverGlobalData.IsInitialized = FALSE;
    }

    return status;
}

/*2*/
STATUS
KeyboardDriverEnable(
    void
)
{
    if (!gKeyboardDriverGlobalData.IsInitialized) { return STATUS_COMPONENT_NOT_INITIALIZED; }

    _KeyboardDriverEnable();

    return STATUS_SUCCESS;
}

/*3*/
void
KeyboardDriverDisable(
    void
)
{
    _KeyboardDriverDisable(TRUE);

    return;
}

/*4*/
STATUS
KeyboardDriverRegisterCallback(
    KeyboardCallbackFunction    Callback,
    BOOLEAN                     TriggerAtSpecificKey,
    CHAR                        Key
)
{
    STATUS status = STATUS_SUCCESS;

    // Disable keyboard driver to avoid inconsistency
    _KeyboardDriverDisable(TRUE);

    if (!gKeyboardDriverGlobalData.IsInitialized)
    {
        status = STATUS_COMPONENT_NOT_INITIALIZED;
        goto cleanup;
    }

    if (!Callback)
    {
        status = STATUS_INVALID_PARAMETER_1;
        goto cleanup;
    }

    if (TriggerAtSpecificKey)
    {
        UNREFERENCED_PARAMETER(Key);
        status = STATUS_NOT_IMPLEMENTED;
        goto cleanup;
    }

    BOOLEAN entryFound = FALSE;
    for (DWORD i = 0; i < MAX_CALLBACKS; ++i)
    {
        if (!gKeyboardDriverGlobalData.Callbacks[i])
        {
            entryFound = TRUE;
            gKeyboardDriverGlobalData.Callbacks[i] = Callback;
            break;
        }
    }

    if (!entryFound)
    {
        status = STATUS_NO_MEMORY_FOUND;
        goto cleanup;
    }

cleanup:
    // Enable keyboard driver
    _KeyboardDriverEnable();

    return status;
}

/*5*/
STATUS
KeyboardDriverUnregisterCallback(
    KeyboardCallbackFunction Callback
)
{
    STATUS status = STATUS_SUCCESS;

    // Disable keyboard driver to avoid inconsistency
    _KeyboardDriverDisable(TRUE);

    if (!gKeyboardDriverGlobalData.IsInitialized)
    {
        status = STATUS_COMPONENT_NOT_INITIALIZED;
        goto cleanup;
    }

    if (!Callback)
    {
        status = STATUS_INVALID_PARAMETER_1;
        goto cleanup;
    }

    for (DWORD i = 0; i < MAX_CALLBACKS; ++i)
    {
        if (gKeyboardDriverGlobalData.Callbacks[i] == Callback)
        {
            gKeyboardDriverGlobalData.Callbacks[i] = NULL;
            break;
        }
    }

cleanup:
    // Enable keyboard driver
    _KeyboardDriverEnable();

    return status;
}

/*6*/
CHAR
KeyboardDriverGetLastKeyPressed(
    void
)
{
    return gKeyboardDriverGlobalData.LastKeyPressed;
}

/* Driver function */
static
STATUS
_KeyboardDriver(
    COMMON_REGISTER_STRUCTURE   *CommonRegisters,
    INTERRUPT_REGISTER_STUCTURE *InterruptRegisters
)
//
// TODO: o mai buna implementare. aici de ex, daca vreau sa scriu accident, nu pot pune
// doi de 'c' ca nu ma lasa codul... Citeste mai bine cum functioneaza keyboard controller-ul!
//
{
    UNREFERENCED_PARAMETER(CommonRegisters);
    UNREFERENCED_PARAMETER(InterruptRegisters);

    BYTE command = __inbyte(0x60);
    CHAR keyPressed = _DecodeKey(command);

    if (!_IsKeyboardDriverEnabled()) { return STATUS_SUCCESS; }

    if (keyPressed == gKeyboardDriverGlobalData.LastKeyPressed) { return STATUS_SUCCESS; }
    gKeyboardDriverGlobalData.LastKeyPressed = keyPressed;

    for (DWORD i = 0; i < MAX_CALLBACKS; ++i)
    {
        if (gKeyboardDriverGlobalData.Callbacks[i]) { gKeyboardDriverGlobalData.Callbacks[i](); }
    }

    return STATUS_SUCCESS;
}

/* Static functions */
static
__forceinline
CHAR
_DecodeKey(
    BYTE Code
)
{
#define LOWER_TO_UPPER_DIFF 0//('a' - 'A')

    Code &= 0x7F;

    switch (Code)
    {
    case 0x1:       return KEY_ESCAPE;
    case 0xE:       return KEY_BACKSPACE;
    case 0x10:      return 'q' - LOWER_TO_UPPER_DIFF;
    case 0x11:      return 'w' - LOWER_TO_UPPER_DIFF;
    case 0x12:      return 'e' - LOWER_TO_UPPER_DIFF;
    case 0x13:      return 'r' - LOWER_TO_UPPER_DIFF;
    case 0x14:      return 't' - LOWER_TO_UPPER_DIFF;
    case 0x15:      return 'y' - LOWER_TO_UPPER_DIFF;
    case 0x16:      return 'u' - LOWER_TO_UPPER_DIFF;
    case 0x17:      return 'i' - LOWER_TO_UPPER_DIFF;
    case 0x18:      return 'o' - LOWER_TO_UPPER_DIFF;
    case 0x19:      return 'p' - LOWER_TO_UPPER_DIFF;
    case 0x1C:      return KEY_ENTER;
    case 0x1E:      return 'a' - LOWER_TO_UPPER_DIFF;
    case 0x1F:      return 's' - LOWER_TO_UPPER_DIFF;
    case 0x20:      return 'd' - LOWER_TO_UPPER_DIFF;
    case 0x21:      return 'f' - LOWER_TO_UPPER_DIFF;
    case 0x22:      return 'g' - LOWER_TO_UPPER_DIFF;
    case 0x23:      return 'h' - LOWER_TO_UPPER_DIFF;
    case 0x24:      return 'j' - LOWER_TO_UPPER_DIFF;
    case 0x25:      return 'k' - LOWER_TO_UPPER_DIFF;
    case 0x26:      return 'l' - LOWER_TO_UPPER_DIFF;
    case 0x2C:      return 'z' - LOWER_TO_UPPER_DIFF;
    case 0x2D:      return 'x' - LOWER_TO_UPPER_DIFF;
    case 0x2E:      return 'c' - LOWER_TO_UPPER_DIFF;
    case 0x2F:      return 'v' - LOWER_TO_UPPER_DIFF;
    case 0x30:      return 'b' - LOWER_TO_UPPER_DIFF;
    case 0x31:      return 'n' - LOWER_TO_UPPER_DIFF;
    case 0x32:      return 'm' - LOWER_TO_UPPER_DIFF;

    default:        return KEY_UNKNOWN;
    }
}

static
__forceinline
void
_KeyboardDriverEnable(
    void
)
{
    PicUnmaskIrqLine(
        PicGetIrqFromInterruptVector(KEYBOARD_DRIVER_INTERRUPT_VECTOR)
    );

    _InterlockedExchange8(&gKeyboardDriverGlobalData.IsKeyboardDriverEnabled, TRUE);
}

static
__forceinline
void
_KeyboardDriverDisable(
    BOOLEAN MaskDriverIrq
)
{
    _InterlockedExchange8(&gKeyboardDriverGlobalData.IsKeyboardDriverEnabled, FALSE);

    if (MaskDriverIrq)
    {
        PicMaskIrqLine(
            PicGetIrqFromInterruptVector(KEYBOARD_DRIVER_INTERRUPT_VECTOR)
        );
    }
}

static
__forceinline
BOOLEAN
_IsKeyboardDriverEnabled(
    void
)
{
    // Cred ca aici ar merge si un if clasic,
    // read-ul dintr-o variabila volatile fiind "interlocked".
    // Dar, ca sa fiu sigur + sa fie mai clar ca variabila trebuie
    // sa fie "modificata cu atentie" o sa folosesc interlocked intrinsic
    return (_InterlockedCompareExchange8(
        &gKeyboardDriverGlobalData.IsKeyboardDriverEnabled,
        gKeyboardDriverGlobalData.IsKeyboardDriverEnabled,
        TRUE) == TRUE ?
        TRUE : FALSE);
}