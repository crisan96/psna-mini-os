#include "driver/timer.h"
#include "driver/driver.h"
#include "io/logging.h"

#define PIT_BINARY_MODE                     (0)
#define PIT_OPERATING_MODE_0                (0)
#define PIT_ACCESS_MODE_LOW_AND_HIGH        (BIT(4) | BIT(5))
#define PIT_CHANNEL_0                       (0)

#define PIT_REG_COMMAND                     0x43
#define PIT_REG_COUNTER0                    0x40

// Pentru ca frecventa PIT-ului care e 1193182 Hz impartita
// la 1194 ne da aprox 1000Hz ceea ce inseamna ca vom primi
// un tick la fiecare milisecunda
#define PIT_INTERRUPT_EVERY_1_MILLISECOND   (1194)

#pragma warning(disable:4324)   // disable structure was padded due to alignment specifier warning
typedef struct _TIMER_DRIVER_GLOBAL_DATA
{
    BOOLEAN                             IsInitialized;

    __declspec (align(8)) volatile char IsTimerDriverEnabled;

    QWORD                               SystemTIcks;
}TIMER_DRIVER_GLOBAL_DATA;
#pragma warning(default:4324)   // enable structure was padded due to alignment specifier warning
static TIMER_DRIVER_GLOBAL_DATA gTimerDriverGlobalData = { 0 };

/* Driver function */
static STATUS _TimerDriver(COMMON_REGISTER_STRUCTURE* CommonRegisters, INTERRUPT_REGISTER_STUCTURE* InterruptRegisters);

/* Static functions */
static __forceinline void       _TimerDriverEnable(void);
static __forceinline void       _TimerDriverDisable(BOOLEAN MaskDriverIrq);
static __forceinline BOOLEAN    _IsTimerDriverEnabled(void);
static __forceinline void       _PitRestoreCounter(void);

/*1*/
STATUS
TimerDriverInit(
    void
)
{
    crt_memset((void*)&gTimerDriverGlobalData, 0, sizeof(gTimerDriverGlobalData));

    _PitRestoreCounter();

    STATUS status = DriverRegister(TIMER_DRIVER_INTERRUPT_VECTOR, _TimerDriver, TimerdDriverDisable);
    if (SUCCESS(status))
    {
        gTimerDriverGlobalData.IsInitialized = TRUE;
        gTimerDriverGlobalData.SystemTIcks = 0;
        _TimerDriverEnable();
    }
    else
    {
        gTimerDriverGlobalData.IsInitialized = FALSE;
    }

    return status;
}

/*2*/
STATUS
TimerdDriverEnable(
    void
)
{
    if (!gTimerDriverGlobalData.IsInitialized) { return STATUS_COMPONENT_NOT_INITIALIZED; }

    _TimerDriverEnable();

    return STATUS_SUCCESS;
}

/*3*/
void
TimerdDriverDisable(
    void
)
{
    _TimerDriverDisable(TRUE);

    return;
}

/*4*/
STATUS
TimerDriverRegisterCallback(
    TimerCallbackFunction   Callback,
    BOOLEAN                 TriggerAtEveryXTicks,
    QWORD                   X
)
{
    UNREFERENCED_PARAMETER(Callback);
    UNREFERENCED_PARAMETER(TriggerAtEveryXTicks);
    UNREFERENCED_PARAMETER(X);

    return STATUS_NOT_IMPLEMENTED;
}

/*5*/
STATUS
TimerDriverUnregisterCallback(
    TimerCallbackFunction Callback
)
{
    UNREFERENCED_PARAMETER(Callback);

    return STATUS_NOT_IMPLEMENTED;
}

/*7*/
QWORD
GetSystemTIcks(
    void
)
{
    return gTimerDriverGlobalData.SystemTIcks;
}

static
STATUS
_TimerDriver(
    COMMON_REGISTER_STRUCTURE* CommonRegisters,
    INTERRUPT_REGISTER_STUCTURE* InterruptRegisters
)
{
    UNREFERENCED_PARAMETER(CommonRegisters);
    UNREFERENCED_PARAMETER(InterruptRegisters);

    if (!_IsTimerDriverEnabled())
    {
        return STATUS_SUCCESS;
    }

    ++gTimerDriverGlobalData.SystemTIcks;

    _PitRestoreCounter();

    return STATUS_SUCCESS;
}

/* Static functions */
static
__forceinline
void
_TimerDriverEnable(
    void
)
{
    PicUnmaskIrqLine(
        PicGetIrqFromInterruptVector(KEYBOARD_DRIVER_INTERRUPT_VECTOR)
    );

    _InterlockedExchange8(&gTimerDriverGlobalData.IsTimerDriverEnabled, TRUE);
}

static
__forceinline
void
_TimerDriverDisable(
    BOOLEAN MaskDriverIrq
)
{
    _InterlockedExchange8(&gTimerDriverGlobalData.IsTimerDriverEnabled, FALSE);

    if (MaskDriverIrq)
    {
        PicMaskIrqLine(
            PicGetIrqFromInterruptVector(TIMER_DRIVER_INTERRUPT_VECTOR)
        );
    }
}

static
__forceinline
BOOLEAN
_IsTimerDriverEnabled(
    void
)
{
    // Cred ca aici ar merge si un if clasic,
    // read-ul dintr-o variabila volatile fiind "interlocked".
    // Dar, ca sa fiu sigur + sa fie mai clar ca variabila trebuie
    // sa fie "modificata cu atentie" o sa folosesc interlocked intrinsic
    return (_InterlockedCompareExchange8(
        &gTimerDriverGlobalData.IsTimerDriverEnabled,
        gTimerDriverGlobalData.IsTimerDriverEnabled,
        TRUE) == TRUE ?
        TRUE : FALSE);
}

static
__forceinline
void
_PitRestoreCounter(
    void
)
{
    __outbyte(PIT_REG_COMMAND, PIT_BINARY_MODE | PIT_OPERATING_MODE_0 | PIT_ACCESS_MODE_LOW_AND_HIGH | PIT_CHANNEL_0 );
    __outbyte(PIT_REG_COUNTER0, WORD_LOW(PIT_INTERRUPT_EVERY_1_MILLISECOND));
    __outbyte(PIT_REG_COUNTER0, WORD_HIGH(PIT_INTERRUPT_EVERY_1_MILLISECOND));

    return;
}