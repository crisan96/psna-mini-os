#include "memory/memory_manager.h"
#include "processor/msr.h"
#include "io/logging.h"
#include "lib/bitmap.h"
#include "processor/paging.h"

#define INITIAL_IMAGE_BASE_PA   (1 * MEGA)      // Adresa initiala unde a fost incarcat kernel-ul.
                                                // Aceasta se va muta (sau nu) la o adresa unde sa avem
                                                // liber destula memorie continua pentru a crea pool-urile de memorie
                                                // necesare (heap, page pool, etc.)
                                                // !ATENTIE!: sa fie tinut sincronizat cu IMAGE_BASE_PA din assembly

#define INITIAL_MAPED_PAGE_POOL (2 * MEGA)      // Am mapat inca din codul de assembly 2MB de memorie din poolul
                                                // de page pool pentru a putea folosi cu usurinta in codul de C functiile
                                                // de paginare.

#define KERNEL_MEMORY           (200 * MEGA)    // Vrem pentru kernel-ul nostru 200MB de memorie.
                                                // Acesti 200MB ii vom imparti in diferite pool-uri de memorie

/*
    The 8-terabyte range from 0x000'00000000 through 0x7FF'FFFFFFFF is used for user space,
    and portions of the 248-terabyte range from 0xFFFF0800'00000000 through 0xFFFFFFFF'FFFFFFFF
    are used for system space.
    And we only support 64 bits.
    Mno, din ceva motiv crapa kernelul imediat dupa mapare si jmp la noul spatiu virtual care incepe
    la 0xFFFF0800'00000000. Totusi, daca incepem spatiul virtual al kernulului la 8 tera (imediat dupa
    user mode) merge. O sa investighez mai tarziu problema dar ma astept sa fie un vmware issue, deci o sa incep
    prin a rula codul pe o masina fizica pt confirmare.
*/
#define KERNEL_MODE_BASE_VIRTUAL_MEM                (0x80000000000)

                                //   200MB  //
/**************************************************************************************/
/* BIN-POOL  |  BITMAPS-POOL  |  PAGE-POOL  |  HEAP-POOL  |  STACKS-POOL |   UNUSED   */
/*   8MB     |        2MB     |      50MB   |     100MB   |     20MB     |    20MB    */
/**************************************************************************************/
#define BIN_POOL_SIZE           (8  * MEGA)
#define BITMAPS_POOL_SIZE       (2  * MEGA) // keep in sync with BITMAPS_POOL_LENGTH from __defs.asm
#define PAGE_POOL_SIZE          (50 * MEGA)
#define HEAP_POOL_SIZE          (100 * MEGA)
#define STACKS_POOL_SIZE        (20 * MEGA)

typedef enum _MEM_POOL_ID
{
    MEM_POOL_ID_BINARY,
    MEM_POOL_ID_BITMAPS,
    MEM_POOL_ID_PAGING,
    MEM_POOL_ID_HEAP,
    MEM_POOL_ID_STACKS,
}MEM_POOL_ID;

typedef struct _MEMORY_POOL
{
    MEM_POOL_ID     Id;
    QWORD           BaseAddressPa;
    QWORD           BaseAddressVa;
    QWORD           Length;

    // For every pool, we have a granularity which describe
    // blocks size. Every pool is divided in blocks with PoolGranularity
    // size. To keep it simple all pools have a default granularity
    // of PAGE_SIZE, just the heap have different. Of course, it would be
    // much better to have more granularities for heap but now we just try
    // to keep it simple.
#define HEAP_GRANULARITY_IN_BYTES               (256)
#define DEFAULT_POOL_GRANULARITY_IN_BYTES       PAGE_SIZE
    QWORD           PoolGranularity;
}MEMORY_POOL;

/*
    For memory allocation, we have to keep additional info so we know
    how to handle memory free.
*/
typedef struct _HEAP_ALLOCATION_ADDITIONAL_INFO
{
    QWORD BitmapIndex;                          // Bitmap index of the first block we allocated
    QWORD BlocksCount;                          // How many blocks we allocated.
}HEAP_ALLOCATION_ADDITIONAL_INFO;

typedef struct _MEM_MANAGER_GLOBAL_DATA
{
    SPINLOCK    MemManagerSpinlock;             // In future, divide synchronization. For example,
                                                // we can use an different sync primitive for the heap pool.

    DWORD       E820KernelIndex;                // Entry-ul in harta de memorie unde noi vom pune kernelul.

    // Pentru a vedea daca mai avem memorie disponibila intr-un pool
    // vom folosi bitmapuri la granularitate de pagina. Mai precis, fiecare
    // pool va fi impartit in numar de pagini de memorie (de 4KB) si se va
    // tine evidenta acestor pagini folosind un bitmap.
    // Deci, va trebui sa cream un bitmap ce va manageria heap-ul, de unde intrebarea
    // cum alocam o zona de memorie pt un bitmap daca nu avem inca heap.
    // Raspunsul: mai facem un pool de bitmapuri. Altfel spus, cum vom avea o zona de memorie
    // speciala pt heap, vom avea si o zona de memorie speciala pt aceste bitmapuri.
    struct
    {
        BITMAP PagePoolBitmap;  // page pool-ul are 50MB => 12800 pagini de memorie
                                // 1 byte din bitmap poate tine evidenta
                                // a 8 pagini de memorie => 12800 / 8 => 1600 bytes

        BITMAP HeapPoolBitmap;  // heap pool-ul este de 100MB => avem nevoie de dublu fata de page pool
                                // => 3200 bytes. Insumati => 4800 bytes => aprox 5 KB
                                // pentru simplitate si pt a da rotund cu pool-ul executabilului (8 + 2 = 10 :D)
                                // plus pt a pastra totul aliniat la pagina vom aloca 1MB pentru cele doua bitmapuri

#define PAGE_POOL_BITMAP_BASE_VA        (gMemManagerGlobalData.MemoryPools.BitmapsPool.BaseAddressVa)
#define PAGE_POOL_BITMAP_NUMBER_OF_BITS (gMemManagerGlobalData.MemoryPools.PagePool.Length / gMemManagerGlobalData.MemoryPools.PagePool.PoolGranularity)

#define HEAP_POOL_BITMAP_BASE_VA        (gMemManagerGlobalData.MemoryPools.BitmapsPool.BaseAddressVa + 1 * MEGA)
#define HEAP_POOL_BITMAP_NUMBER_OF_BITS (gMemManagerGlobalData.MemoryPools.HeapPool.Length / gMemManagerGlobalData.MemoryPools.HeapPool.PoolGranularity)
    }PoolBitmaps;

    struct _MEMORY_POOLS
    {
        MEMORY_POOL BinaryPool;
        MEMORY_POOL BitmapsPool;
        MEMORY_POOL PagePool;
        MEMORY_POOL HeapPool;
        MEMORY_POOL StacksPool;
    }MemoryPools; // TODO: Ar trebui sa packam structura? vezi functia _GetMemoryPoolByPhysicallAddress
#define NUMBERS_OF_MEMORY_POOLS (sizeof(struct _MEMORY_POOLS) / sizeof(MEMORY_POOL))

}MEM_MANAGER_GLOBAL_DATA;
static MEM_MANAGER_GLOBAL_DATA gMemManagerGlobalData;

/* Static functions */
static BOOLEAN      _IsPaInsideKernelMemory(QWORD Pa);
static void         _ConvertBitmapIndexToPaAndVa(MEMORY_POOL* MemPool, QWORD Index, QWORD* Pa, QWORD* Va);
static STATUS       _MemManagerInitMoveBinary(void);
static STATUS       _MemManagerInitDoNotMoveBinary(void);
static void         _InitPools(QWORD BaseAddressPa, QWORD BaseAddressVa);
static MEMORY_POOL* _GetMemoryPoolByPhysicallAddress(QWORD Pa);

/*1*/
STATUS
MemManagerInit(
    void
)
//
// Probabil cea mai urata functie din tot proiectul.
// Aici vom vrea sa initializam management-ul memoriei. Asta presupune cautarea unei zone de memorie
// de 8MB (dimensiunea executabilului) + 2MB (pool-ul pentru bitmapuri, vezi mai sus explicatia)
// + 50MB (pool-ul de memorie ce-l vom folosi pentru structuri de
// paginare) + 100MB (ce-l vom folosi ca si heap) + 20MB (ce va fi folosit pentru stack-urilor
// procesoarelor ce le vom trezi, AP-urilor) => 128MB
//
{
    STATUS status = SpinlockInit(&gMemManagerGlobalData.MemManagerSpinlock);
    if (!SUCCESS(status)) { return status; }

    //
    // Search for a memory map entry with 200MB free space
    //
    BOOLEAN entryFound = FALSE;
    for (DWORD i = 0; i < KernelGlobalData.AsmData->MemoryMap.NumberOfEntries; ++i)
    {
        if ((KernelGlobalData.AsmData->MemoryMap.MmapEntry[i].Type == MEMORY_TYPE_USBALE)
            && (KernelGlobalData.AsmData->MemoryMap.MmapEntry[i].Length >= KERNEL_MEMORY)
            && (KernelGlobalData.AsmData->MemoryMap.MmapEntry[i].BaseAddress % PAGE_SIZE == 0)
            )
        {
            gMemManagerGlobalData.E820KernelIndex = i;
            entryFound = TRUE;
            break;
        }
    }

    if (!entryFound) { return STATUS_NO_MEMORY_FOUND; }

    //
    // Initialize memory pools info
    //
    MMAP_ENTRY* kernelMmapEntry = &KernelGlobalData.AsmData->MemoryMap.MmapEntry[gMemManagerGlobalData.E820KernelIndex];
    _InitPools(kernelMmapEntry->BaseAddress, KERNEL_MODE_BASE_VIRTUAL_MEM);

    //
    // Initialize bitmaps
    //
    BitmapEarlyInit(
        &gMemManagerGlobalData.PoolBitmaps.PagePoolBitmap,
        PAGE_POOL_BITMAP_BASE_VA,
        PAGE_POOL_BITMAP_NUMBER_OF_BITS
    );
    BitmapEarlyInit(
        &gMemManagerGlobalData.PoolBitmaps.HeapPoolBitmap,
        HEAP_POOL_BITMAP_BASE_VA,
        HEAP_POOL_BITMAP_NUMBER_OF_BITS
    );

    //
    // Init paging entity
    //
    status = PagingInit();
    if (!SUCCESS(status)) { return status; }

    if (gMemManagerGlobalData.MemoryPools.BinaryPool.BaseAddressPa == INITIAL_IMAGE_BASE_PA)
    {
        // In acest caz intrarea gasita in memoria RAM
        // este chiar cea la care am pornit. Deci nu trebuie sa
        // mutam binarul in alta zona de memorie.
        return _MemManagerInitDoNotMoveBinary();
    }
    else
    {
        // In acest caz intrarea gasita in memoria RAM
        // nu este cea in care am inceput. Deci, va trebui
        // sa mutam binarul in alta zona de memorie
        return _MemManagerInitMoveBinary();
    }
}

/*2*/
STATUS
MemManagerPaToVa(
    QWORD   Pa,
    QWORD   *Va
)
{
    if (!Va) { return STATUS_INVALID_PARAMETER_2; }

    MEMORY_POOL* memoryPoolContainingAddress = _GetMemoryPoolByPhysicallAddress(Pa);
    if (!memoryPoolContainingAddress) { return STATUS_INVALID_PARAMETER_1; }

    // We only support giving physical address of the page pool pages
    if (memoryPoolContainingAddress->Id != MEM_POOL_ID_PAGING) { return STATUS_NOT_SUPPORTED; }

    QWORD offsetInPool = Pa - memoryPoolContainingAddress->BaseAddressPa;
    *Va = memoryPoolContainingAddress->BaseAddressVa + offsetInPool;

    return STATUS_SUCCESS;
}

/*3*/
STATUS
MemManagerAllocPageForPagingStructure(
    QWORD* Pa
)
{
    STATUS status;
    SpinlockAquire(&gMemManagerGlobalData.MemManagerSpinlock);

    // Get first free page from page pool
    QWORD bitmapIndexOfFreePage;
    status = BitmapScanMultiple(
        &gMemManagerGlobalData.PoolBitmaps.PagePoolBitmap,  // page pool bitmap
        1,                                                  // we search for the first page
        BIT_STATUS_FREE,                                    // first free page
        &bitmapIndexOfFreePage
    );
    if (!SUCCESS(status)) { goto cleanup; }

    // Get Pa and Va of the first free page from page pool
    QWORD pageStructureVa;
    _ConvertBitmapIndexToPaAndVa(
        &gMemManagerGlobalData.MemoryPools.PagePool,
        bitmapIndexOfFreePage,
        Pa,
        &pageStructureVa
    );

    // Set the page to zero
    crt_memset((void*)pageStructureVa, 0, PAGE_SIZE);

    // Set the page as used in the bitmap
    status = BitmapSet(
        &gMemManagerGlobalData.PoolBitmaps.PagePoolBitmap,  // page pool bitmap
        bitmapIndexOfFreePage,                              // page index in the bitmap
        BIT_STATUS_USED                                     // set to used
    );
    if (!SUCCESS(status)) { goto cleanup; }

cleanup:
    SpinlockRelease(&gMemManagerGlobalData.MemManagerSpinlock);
    return status;
}

/*4*/
void*
MemManagerAlloc(
    QWORD Size
)
{
    QWORD returnedVa = NULL;
    SpinlockAquire(&gMemManagerGlobalData.MemManagerSpinlock);

    // Actual size is the Size required + size of additional info per allocation
    HEAP_ALLOCATION_ADDITIONAL_INFO heapAllocationAdditionalInfo;
    QWORD actualSize = sizeof(heapAllocationAdditionalInfo) + Size;

    // How many blocks the actualSize required based on heap pool granularity
    QWORD memoryBlocksRequired = DIV_ROUND_UP(actualSize, gMemManagerGlobalData.MemoryPools.HeapPool.PoolGranularity);

    QWORD bitmapIndex;
    STATUS status = BitmapScanMultiple(
        &gMemManagerGlobalData.PoolBitmaps.HeapPoolBitmap,  // Search from heap pool
        memoryBlocksRequired,                               // if there are memoryBlocksRequired blocks
        BIT_STATUS_FREE,                                    // with status FREE.
        &bitmapIndex
    );
    if (!SUCCESS(status))
    {
        returnedVa = NULL;
        goto cleanup;
    }

    // Mark those blocks as USED
    status = BitmapSetMultiple(
        &gMemManagerGlobalData.PoolBitmaps.HeapPoolBitmap,
        bitmapIndex,
        memoryBlocksRequired,
        BIT_STATUS_USED
    );
    if (!SUCCESS(status))
    {
        returnedVa = NULL;
        goto cleanup;
    }

    // Compute heap virtual address from bitmap index
    _ConvertBitmapIndexToPaAndVa(
        &gMemManagerGlobalData.MemoryPools.HeapPool,
        bitmapIndex,
        NULL,
        &returnedVa
    );

    // Compute additional heap info per allocation
    // and save it at the start of the virtual addr
    heapAllocationAdditionalInfo.BitmapIndex = bitmapIndex;
    heapAllocationAdditionalInfo.BlocksCount = memoryBlocksRequired;
    crt_memcpy((void*)returnedVa, (void*)&heapAllocationAdditionalInfo, sizeof(heapAllocationAdditionalInfo));

    // Make virtual address point at the very first byte after additional info struct
    returnedVa += sizeof(heapAllocationAdditionalInfo);

cleanup:
    SpinlockRelease(&gMemManagerGlobalData.MemManagerSpinlock);
    return (void*)returnedVa;
}

/*5*/
void
MemManagerFree(
    void* Address
)
{
    SpinlockAquire(&gMemManagerGlobalData.MemManagerSpinlock);

    // Find the additional info saved for this address
    HEAP_ALLOCATION_ADDITIONAL_INFO *heapAllocationAdditionalInfo
        = (HEAP_ALLOCATION_ADDITIONAL_INFO*) ((QWORD)Address - sizeof(HEAP_ALLOCATION_ADDITIONAL_INFO));

    // Set this memory as free in the bitmap
    STATUS status = BitmapSetMultiple(
        &gMemManagerGlobalData.PoolBitmaps.HeapPoolBitmap,
        heapAllocationAdditionalInfo->BitmapIndex,
        heapAllocationAdditionalInfo->BlocksCount,
        BIT_STATUS_FREE
    );
    if (!SUCCESS(status)) { goto cleanup; }

cleanup:
    SpinlockRelease(&gMemManagerGlobalData.MemManagerSpinlock);
    return;
}

/* Static functions */
static
BOOLEAN
_IsPaInsideKernelMemory(
    QWORD Pa
)
{
    MMAP_ENTRY* kernelMmapEntry = &KernelGlobalData.AsmData->MemoryMap.MmapEntry[gMemManagerGlobalData.E820KernelIndex];

    if ((Pa >= kernelMmapEntry->BaseAddress)
        && (Pa < kernelMmapEntry->BaseAddress + kernelMmapEntry->Length)
        )
    {
        return TRUE;
    }

    return FALSE;
}

static
void
_ConvertBitmapIndexToPaAndVa(
    MEMORY_POOL *MemPool,
    QWORD       Index,
    QWORD       *Pa,
    QWORD       *Va
)
{
    if (Pa) { *Pa = MemPool->BaseAddressPa + (Index * MemPool->PoolGranularity); }
    if (Va) { *Va = MemPool->BaseAddressVa + (Index * MemPool->PoolGranularity); }

    return;
}

static
STATUS
_MemManagerInitMoveBinary(
    void
)
//
// Suntem in cazul in care zona gasita in memoria RAM NU este
// cea in care cam inceput procesul de boot. In acest caz, trebuie
// sa mutam tot binarul la alta adresa FIZICA de memorie (dar vom
// pastra evident aceleasi adrese virtuale!).
//
{
    STATUS status = STATUS_SUCCESS;

    // Salvam adresa fizica a page pool-ului si reustauram cea originala, de la boot,
    // pentru ca acel page pool il vom folosi deocamdata pentru mapari
    QWORD savedPagePoolBasePa = gMemManagerGlobalData.MemoryPools.PagePool.BaseAddressPa;
    QWORD originalPagePoolBasePa = INITIAL_IMAGE_BASE_PA + BIN_POOL_SIZE + BITMAPS_POOL_SIZE;
    gMemManagerGlobalData.MemoryPools.PagePool.BaseAddressPa = originalPagePoolBasePa;

    // We already use pagin pool from assembly code so we want
    // to not overwrite those paging structure created there.
    // To make that possible, we transfer from asselbly code
    // the physical address of the last page structure used
    // so we will mark the page pool as used from the beggining
    // until we reach that page (in other words, we mark used pages
    // from page pool as used :D )
    QWORD pageNumberOfTheLastPageUsedFromPagePool =
        (KernelGlobalData.AsmData->PagingStructureNextEntry
            - gMemManagerGlobalData.MemoryPools.PagePool.BaseAddressPa)
        / PAGE_SIZE;

    status = BitmapSetMultiple(
        &gMemManagerGlobalData.PoolBitmaps.PagePoolBitmap,
        0,
        pageNumberOfTheLastPageUsedFromPagePool + 1,
        BIT_STATUS_USED
    );
    if (!SUCCESS(status)) { return status; }

#define START_OF_KERNEL_DUMMY_ADDR  (1 * TERA)  // Inceputul adresei virtuale prin care vom avea acces
                                                // la noua memorie fizica unde va fi mutat kernelul

    // Mapam prin adresa dummy poolul de binar
    // pentru ca mai tarziu sa-l putem muta acolo
    QWORD currentPoolOffset = 0;
    status = PagingMapVaToPaRangeQuick(
        START_OF_KERNEL_DUMMY_ADDR + currentPoolOffset,
        gMemManagerGlobalData.MemoryPools.BinaryPool.BaseAddressPa,
        gMemManagerGlobalData.MemoryPools.BinaryPool.Length / PAGE_SIZE,
        ACCESS_RIGHT_WRITE | ACCES_RIGHT_EXECUTE
    );
    if (!SUCCESS(status)) { return status; }

    // Copiem binarul la noua adresa fizica la care avem acces prin Va-ul dummy
    crt_memcpy((void*)START_OF_KERNEL_DUMMY_ADDR, (void*)KERNEL_MODE_BASE_VIRTUAL_MEM, BIN_POOL_SIZE);

    // Mapam si bitmaps pool-ul pentru ca bitmap-ul ce gestioneaza
    // page pool-ul este folosit la mapare. Il vom initializa gol.
    // Noi vom incepe sa creem un cr3 in noul page pool si sa mapam zona noua de memorie
    currentPoolOffset += gMemManagerGlobalData.MemoryPools.BinaryPool.Length;
    QWORD dummyBitmapsPoolVa = START_OF_KERNEL_DUMMY_ADDR + currentPoolOffset;
    status = PagingMapVaToPaRangeQuick(
        dummyBitmapsPoolVa,
        gMemManagerGlobalData.MemoryPools.BitmapsPool.BaseAddressPa,
        gMemManagerGlobalData.MemoryPools.BitmapsPool.Length / PAGE_SIZE,
        ACCESS_RIGHT_WRITE
    );
    if (!SUCCESS(status)) { return status; }

    // Evident, mapam si noul page pool ca doar aici vom construi
    // noile tabele de paginare :)
    currentPoolOffset += gMemManagerGlobalData.MemoryPools.BitmapsPool.Length;
    QWORD dummyPagePoolBaseVa = START_OF_KERNEL_DUMMY_ADDR + currentPoolOffset;
    status = PagingMapVaToPaRangeQuick(
        dummyPagePoolBaseVa,
        savedPagePoolBasePa,
        gMemManagerGlobalData.MemoryPools.PagePool.Length / PAGE_SIZE,
        ACCESS_RIGHT_WRITE
    );
    if (!SUCCESS(status)) { return status; }

    // Setam bitmapul de pagini ca fiind gol
    QWORD savedBitmapsPoolVa = gMemManagerGlobalData.MemoryPools.BitmapsPool.BaseAddressVa;
    gMemManagerGlobalData.MemoryPools.BitmapsPool.BaseAddressVa = dummyBitmapsPoolVa;
    BitmapEarlyInit(
        &gMemManagerGlobalData.PoolBitmaps.PagePoolBitmap,
        PAGE_POOL_BITMAP_BASE_VA,
        PAGE_POOL_BITMAP_NUMBER_OF_BITS
    );

    gMemManagerGlobalData.MemoryPools.PagePool.BaseAddressPa = savedPagePoolBasePa;
    QWORD savedPagePoolBaseVa = gMemManagerGlobalData.MemoryPools.PagePool.BaseAddressVa;
    gMemManagerGlobalData.MemoryPools.PagePool.BaseAddressVa = dummyPagePoolBaseVa;

    //
    // Incepem sa paginam tot kernelul la adresa noua
    // Cream un nou CR3
    //
    QWORD newCr3;
    status = PagingMapVaToPaRange(
        &newCr3,
        TRUE,
        gMemManagerGlobalData.MemoryPools.BinaryPool.BaseAddressVa,
        gMemManagerGlobalData.MemoryPools.BinaryPool.BaseAddressPa,
        gMemManagerGlobalData.MemoryPools.BinaryPool.Length / PAGE_SIZE,
        ACCESS_RIGHT_WRITE | ACCES_RIGHT_EXECUTE
    );
    if (!SUCCESS(status)) { return status; }

    status = PagingMapVaToPaRange(
        &newCr3,
        FALSE,
        savedBitmapsPoolVa,
        gMemManagerGlobalData.MemoryPools.BitmapsPool.BaseAddressPa,
        gMemManagerGlobalData.MemoryPools.BitmapsPool.Length / PAGE_SIZE,
        ACCESS_RIGHT_WRITE
    );
    if (!SUCCESS(status)) { return status; }

    status = PagingMapVaToPaRange(
        &newCr3,
        FALSE,
        savedPagePoolBaseVa,
        gMemManagerGlobalData.MemoryPools.PagePool.BaseAddressPa,
        gMemManagerGlobalData.MemoryPools.PagePool.Length / PAGE_SIZE,
        ACCESS_RIGHT_WRITE
    );
    if (!SUCCESS(status)) { return status; }

    status = PagingMapVaToPaRange(
        &newCr3,
        FALSE,
        gMemManagerGlobalData.MemoryPools.HeapPool.BaseAddressVa,
        gMemManagerGlobalData.MemoryPools.HeapPool.BaseAddressPa,
        gMemManagerGlobalData.MemoryPools.HeapPool.Length / PAGE_SIZE,
        ACCESS_RIGHT_WRITE
    );
    if (!SUCCESS(status)) { return status; }

    status = PagingMapVaToPaRange(
        &newCr3,
        FALSE,
        gMemManagerGlobalData.MemoryPools.StacksPool.BaseAddressVa,
        gMemManagerGlobalData.MemoryPools.StacksPool.BaseAddressPa,
        gMemManagerGlobalData.MemoryPools.StacksPool.Length / PAGE_SIZE,
        ACCESS_RIGHT_WRITE
    );
    if (!SUCCESS(status)) { return status; }

    gMemManagerGlobalData.MemoryPools.BitmapsPool.BaseAddressVa = savedBitmapsPoolVa;
    gMemManagerGlobalData.MemoryPools.PagePool.BaseAddressVa = savedPagePoolBaseVa;

    __writecr3(newCr3);

    // Initialize heap bitmap
    BitmapEarlyInit(
        &gMemManagerGlobalData.PoolBitmaps.HeapPoolBitmap,
        HEAP_POOL_BITMAP_BASE_VA,
        HEAP_POOL_BITMAP_NUMBER_OF_BITS
    );

    return STATUS_NOT_IMPLEMENTED;
}

static
STATUS
_MemManagerInitDoNotMoveBinary(
    void
)
//
// Suntem in cazul in care zona gasita in memoria RAM este
// chiar zona in care am inceput procesul de boot. Deci,
// nu mai trebuie sa mutam binarul in alta zona de memorie ci doar sa
// terminam de mapat de am inceput in codul de assembly.
//
{
    STATUS status = STATUS_SUCCESS;

    // We already use pagin pool from assembly code so we want
    // to not overwrite those paging structure created there.
    // To make that possible, we transfer from asselbly code
    // the physical address of the last page structure used
    // so we will mark the page pool as used from the beggining
    // until we reach that page (in other words, we mark used pages
    // from page pool as used :D )
    QWORD pageNumberOfTheLastPageUsedFromPagePool =
        (KernelGlobalData.AsmData->PagingStructureNextEntry
        - gMemManagerGlobalData.MemoryPools.PagePool.BaseAddressPa)
        / PAGE_SIZE;

    status = BitmapSetMultiple(
        &gMemManagerGlobalData.PoolBitmaps.PagePoolBitmap,
        0,
        pageNumberOfTheLastPageUsedFromPagePool + 1,
        BIT_STATUS_USED
    );
    if (!SUCCESS(status)) { return status; }

    // Pool-urile de binar, de bitmapuri si INITIAL_MAPED_PAGE_POOL bytes
    // din pool-ul de pagini au fost mapate. Trebuie sa mapam restul
    status = PagingMapVaToPaRangeQuick(
        gMemManagerGlobalData.MemoryPools.PagePool.BaseAddressVa + INITIAL_MAPED_PAGE_POOL,
        gMemManagerGlobalData.MemoryPools.PagePool.BaseAddressPa + INITIAL_MAPED_PAGE_POOL,
        (gMemManagerGlobalData.MemoryPools.PagePool.Length - INITIAL_MAPED_PAGE_POOL) / PAGE_SIZE,
        ACCESS_RIGHT_WRITE
    );
    if (!SUCCESS(status)) { return status; }

    status = PagingMapVaToPaRangeQuick(
        gMemManagerGlobalData.MemoryPools.HeapPool.BaseAddressVa,
        gMemManagerGlobalData.MemoryPools.HeapPool.BaseAddressPa,
        gMemManagerGlobalData.MemoryPools.HeapPool.Length / PAGE_SIZE,
        ACCESS_RIGHT_WRITE
    );
    if (!SUCCESS(status)) { return status; }

    status = PagingMapVaToPaRangeQuick(
        gMemManagerGlobalData.MemoryPools.StacksPool.BaseAddressVa,
        gMemManagerGlobalData.MemoryPools.StacksPool.BaseAddressPa,
        gMemManagerGlobalData.MemoryPools.StacksPool.Length / PAGE_SIZE,
        ACCESS_RIGHT_WRITE
    );
    if (!SUCCESS(status)) { return status; }

    return status;
}

static
void
_InitPools(
    QWORD BaseAddressPa,
    QWORD BaseAddressVa
)
{
    // Binary pool
    gMemManagerGlobalData.MemoryPools.BinaryPool.Id = MEM_POOL_ID_BINARY;
    gMemManagerGlobalData.MemoryPools.BinaryPool.BaseAddressPa = BaseAddressPa;
    gMemManagerGlobalData.MemoryPools.BinaryPool.BaseAddressVa = BaseAddressVa;
    gMemManagerGlobalData.MemoryPools.BinaryPool.Length = BIN_POOL_SIZE;
    gMemManagerGlobalData.MemoryPools.BinaryPool.PoolGranularity = DEFAULT_POOL_GRANULARITY_IN_BYTES;
    // Bitmap pool
    gMemManagerGlobalData.MemoryPools.BitmapsPool.Id = MEM_POOL_ID_BITMAPS;
    gMemManagerGlobalData.MemoryPools.BitmapsPool.BaseAddressPa =
        gMemManagerGlobalData.MemoryPools.BinaryPool.BaseAddressPa + gMemManagerGlobalData.MemoryPools.BinaryPool.Length;
    gMemManagerGlobalData.MemoryPools.BitmapsPool.BaseAddressVa =
        gMemManagerGlobalData.MemoryPools.BinaryPool.BaseAddressVa + gMemManagerGlobalData.MemoryPools.BinaryPool.Length;
    gMemManagerGlobalData.MemoryPools.BitmapsPool.Length = BITMAPS_POOL_SIZE;
    gMemManagerGlobalData.MemoryPools.BitmapsPool.PoolGranularity = DEFAULT_POOL_GRANULARITY_IN_BYTES;
    // Page pool
    gMemManagerGlobalData.MemoryPools.PagePool.Id = MEM_POOL_ID_PAGING;
    gMemManagerGlobalData.MemoryPools.PagePool.BaseAddressPa =
        gMemManagerGlobalData.MemoryPools.BitmapsPool.BaseAddressPa + gMemManagerGlobalData.MemoryPools.BitmapsPool.Length;
    gMemManagerGlobalData.MemoryPools.PagePool.BaseAddressVa =
        gMemManagerGlobalData.MemoryPools.BitmapsPool.BaseAddressVa + gMemManagerGlobalData.MemoryPools.BitmapsPool.Length;
    gMemManagerGlobalData.MemoryPools.PagePool.Length = PAGE_POOL_SIZE;
    gMemManagerGlobalData.MemoryPools.PagePool.PoolGranularity = DEFAULT_POOL_GRANULARITY_IN_BYTES;
    // Heap pool
    gMemManagerGlobalData.MemoryPools.HeapPool.Id = MEM_POOL_ID_HEAP;
    gMemManagerGlobalData.MemoryPools.HeapPool.BaseAddressPa =
        gMemManagerGlobalData.MemoryPools.PagePool.BaseAddressPa + gMemManagerGlobalData.MemoryPools.PagePool.Length;
    gMemManagerGlobalData.MemoryPools.HeapPool.BaseAddressVa =
        gMemManagerGlobalData.MemoryPools.PagePool.BaseAddressVa + gMemManagerGlobalData.MemoryPools.PagePool.Length;
    gMemManagerGlobalData.MemoryPools.HeapPool.Length = HEAP_POOL_SIZE;
    gMemManagerGlobalData.MemoryPools.HeapPool.PoolGranularity = HEAP_GRANULARITY_IN_BYTES;
    // Stacks pool
    gMemManagerGlobalData.MemoryPools.StacksPool.Id = MEM_POOL_ID_STACKS;
    gMemManagerGlobalData.MemoryPools.StacksPool.BaseAddressPa =
        gMemManagerGlobalData.MemoryPools.HeapPool.BaseAddressPa + gMemManagerGlobalData.MemoryPools.HeapPool.Length;
    gMemManagerGlobalData.MemoryPools.StacksPool.BaseAddressVa =
        gMemManagerGlobalData.MemoryPools.HeapPool.BaseAddressVa + gMemManagerGlobalData.MemoryPools.HeapPool.Length;
    gMemManagerGlobalData.MemoryPools.StacksPool.Length = STACKS_POOL_SIZE;
    gMemManagerGlobalData.MemoryPools.StacksPool.PoolGranularity = DEFAULT_POOL_GRANULARITY_IN_BYTES;

    return;
}

static
MEMORY_POOL*
_GetMemoryPoolByPhysicallAddress(
    QWORD Pa
)
{
    QWORD i;
    MEMORY_POOL* memPool;
    for (memPool = &gMemManagerGlobalData.MemoryPools.BinaryPool, i = 0;
        i < NUMBERS_OF_MEMORY_POOLS; memPool++, i++)
    {
        if (Pa >= memPool->BaseAddressPa && Pa < memPool->BaseAddressPa + memPool->Length)
            return memPool;
    }

    return NULL;
}