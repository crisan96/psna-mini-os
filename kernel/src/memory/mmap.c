#include "memory/mmap.h"
#include "io/logging.h"
#include "global/global.h"

/*1*/
void
MmapDump(
    void
)
{
    LOG_INFO("------------------------------- Memory map start DUMP ---------------------------------\n");
    for (DWORD i = 0; i < KernelGlobalData.AsmData->MemoryMap.NumberOfEntries; ++i)
    {
        LOG_INFO("Index %d: Base Addr = 0x%x, Length = 0x%x, Type = %d\n",
            (QWORD)i,
            (QWORD)KernelGlobalData.AsmData->MemoryMap.MmapEntry[i].BaseAddress,
            (QWORD)KernelGlobalData.AsmData->MemoryMap.MmapEntry[i].Length,
            (QWORD)KernelGlobalData.AsmData->MemoryMap.MmapEntry[i].Type
        );
    }
    LOG_INFO("------------------------------- Memory map  end DUMP ---------------------------------\n");
}