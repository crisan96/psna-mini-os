; Function where all interrupts will jump
extern InterruptHandler

[bits 64]

%assign i 0
%rep 256
    global __Handler %+ i
    __Handler %+ i %+ :
    push    rax
    push    i
    jmp     ExecuteInterrupt
%assign i i+1
%endrep

ExecuteInterrupt:
    push    rbp
    push    rbx
    push    rcx
    push    rdx
    push    rsi
    push    rdi
    push    r8
    push    r9
    push    r10
    push    r11
    push    r12
    push    r13
    push    r14
    push    r15
    mov     rax, ds
    push    rax
    mov     rax, es
    push    rax
    mov     rax, fs
    push    rax
    mov     rax, gs
    push    rax
    mov     rax, cr0
    push    rax
    mov     rax, cr2
    push    rax
    mov     rax, cr3
    push    rax
    mov     rcx, rsp
    sub     rsp, 0x20
    call    InterruptHandler
    add     rsp, 0x20
    pop     rax
    mov     cr3, rax
    pop     rax
    ;mov    cr2, rax
    pop     rax
    mov     cr0, rax
    pop     rax ;;gs
    pop     rax ;;fs
    pop     rax ;;es
    pop     rax ;;ds
    pop     r15
    pop     r14
    pop     r13
    pop     r12
    pop     r11
    pop     r10
    pop     r9
    pop     r8
    pop     rdi
    pop     rsi
    pop     rdx
    pop     rcx
    pop     rbx
    pop     rbp
    pop     rax ;;-> pop VectorNumber
    pop     rax
    iretq