#include "processor/cpu.h"
#include "memory/memory_manager.h"
#include "processor/msr.h"

/*1*/
STATUS
CpuEnableSSE(
    void
)
//
// Maybe its a good ideea to move this into asm
// cuz compiler can generate code who use sse before we enable it :-?
// If we move this to asembly, we are sure we do not use sse before enable it
//
{
    // To check for SSE CPUID.01h:EDX.SSE[bit 25] needs to be set
    int cpuidRes[4];
    __cpuid(cpuidRes, 1);

    DWORD edx = cpuidRes[3];

    if (!(edx & BIT(25))) { return STATUS_NOT_SUPPORTED; }

    // In order to allow SSE instructions to be executed without generating a #UD,
    // we need to alter the CR0 and CR4 registers as follow:
    QWORD cr0 = __readcr0();
    cr0 &= ~BIT(2); // clear the CR0.EM bit (bit 2)
    cr0 |= BIT(1);  // set the CR0.MP bit (bit 1)
    __writecr0(cr0);

    // Set the CR4.OSFXSR bit (bit 9) && set the CR4.OSXMMEXCPT bit (bit 10)
    QWORD cr4 = __readcr4();
    cr4 |= BIT(9) | BIT(10);
    __writecr4(cr4);

    return STATUS_SUCCESS;
}

/*2*/
STATUS
CpuSetRegistersForCurrentCpu(
    void
)
{
    STATUS status = CpuEnableSSE();
    if (!SUCCESS(status))
    {
        return status;
    }

    // For highest processor performance, both the CD and the NW flags
    // in control register CR0 should be cleared.
    QWORD cr0 = __readcr0();
    cr0 &= ~BIT(30); // clear CD
    cr0 &= ~BIT(29); // crear NW
    __writecr0(cr0);

    if (__readmsr(IA32_PAT) != EXPECTED_IA32_PAT_VALUES)
    {
        __writemsr(IA32_PAT, EXPECTED_IA32_PAT_VALUES);
    }

    // Enables page access restriction by preventing instruction fetches
    // from pages with the XD bit set
    IA32_EFER_REGISTER ia32EferRegister;
    ia32EferRegister.Raw = __readmsr(IA32_EFER);
    ia32EferRegister.ExecuteDisableBitEnable = TRUE;
    __writemsr(IA32_EFER, ia32EferRegister.Raw);

    return STATUS_SUCCESS;
}

/*3*/
void
CpuReset(
    void
)
//
// Aici ar trebui sa luam in ordine de la cea mai buna
// metoda pana la cea mai putin buna
//
{
    // Hard reset
    __outbyte(0x64, 0xFE);
}