#include "processor/idt.h"
#include "lib/std.h"
#include "io/logging.h"
#include "driver/driver.h"

/* The base addresses of the IDT should be aligned on an 8-byte(16-byte) boundary
to maximize performance of cache line fills. */
#define IDT_ALIGN                   __declspec(align(16))

typedef enum _EXCEPTION
{
    EXCEPTION_DIVIDE_ERROR,
    EXCEPTION_DEBUG_EXCEPTION,
    EXCEPTION_NMI_INTERRUPT,
    EXCEPTION_BREAKPOINT,
    EXCEPTION_OVERFLOW,
    EXCEPTION_BOUND_RANGE_EXCEEDED,
    EXCEPTION_INVALID_OPCODE,
    EXCEPTION_DEVICE_NOT_AVAILABLE,
    EXCEPTION_DOUBLE_FAULT                  = 8,
    EXCEPTION_INVALID_TSS                   = 10,
    EXCEPTION_SEGMENT_NOT_PRESENT,
    EXCEPTION_STACK_SEGMENT_FAULT,
    EXCEPTION_GENERAL_PROTECTION,
    EXCEPTION_PAGE_FAULT                    = 14,
    EXCEPTION_FLOATING_POINT_ERROR          = 16,
    EXCEPTION_ALIGNMENT_CHECK,
    EXCEPTION_MACHINE_CHECK,
    EXCEPTION_FLOATING_POINT_EXCEPTION,
    EXCEPTION_VIRTUALIZATION_EXCEPTION,
    EXCEPTION_USER_DEFINED_START            = 32,
    EXCEPTION_USER_DEFINED_END              = 255
}EXCEPTION;

typedef struct _EXCEPTION_INFO
{
    CHAR*   Name;
    BOOLEAN HasErrorCode;
}EXCEPTION_INFO;
static EXCEPTION_INFO gExceptionsInfo[IDT_MAX_ENTRIES] =
{
    [EXCEPTION_DIVIDE_ERROR] =              { .Name = "Divide Error",                     .HasErrorCode = FALSE },
    [EXCEPTION_DEBUG_EXCEPTION] =           { .Name = "Debug Exception",                  .HasErrorCode = FALSE },
    [EXCEPTION_NMI_INTERRUPT] =             { .Name = "NMI Interrupt",                    .HasErrorCode = FALSE },
    [EXCEPTION_BREAKPOINT] =                { .Name = "Breakpoint",                       .HasErrorCode = FALSE },
    [EXCEPTION_OVERFLOW] =                  { .Name = "Overflow",                         .HasErrorCode = FALSE },
    [EXCEPTION_BOUND_RANGE_EXCEEDED] =      { .Name = "Bound Range Exceeded",             .HasErrorCode = FALSE },
    [EXCEPTION_INVALID_OPCODE] =            { .Name = "Invalid Opcode",                   .HasErrorCode = FALSE },
    [EXCEPTION_DEVICE_NOT_AVAILABLE] =      { .Name = "Device Not Available",             .HasErrorCode = FALSE },
    [EXCEPTION_DOUBLE_FAULT] =              { .Name = "Double Fault",                     .HasErrorCode = TRUE  },
    [EXCEPTION_INVALID_TSS] =               { .Name = "Invalid TSS",                      .HasErrorCode = TRUE  },
    [EXCEPTION_SEGMENT_NOT_PRESENT] =       { .Name = "Segment Not Present",              .HasErrorCode = TRUE  },
    [EXCEPTION_STACK_SEGMENT_FAULT] =       { .Name = "Stack-Segment Fault",              .HasErrorCode = TRUE  },
    [EXCEPTION_GENERAL_PROTECTION] =        { .Name = "General Protection",               .HasErrorCode = TRUE  },
    [EXCEPTION_PAGE_FAULT] =                { .Name = "Page Fault",                       .HasErrorCode = TRUE  },
    [EXCEPTION_FLOATING_POINT_ERROR] =      { .Name = "Math Fault",                       .HasErrorCode = FALSE },
    [EXCEPTION_ALIGNMENT_CHECK] =           { .Name = "Alignment Check",                  .HasErrorCode = TRUE  },
    [EXCEPTION_MACHINE_CHECK] =             { .Name = "Machine Check",                    .HasErrorCode = FALSE },
    [EXCEPTION_FLOATING_POINT_EXCEPTION] =  { .Name = "SIMD Floating-Point Exception",    .HasErrorCode = FALSE },
    [EXCEPTION_VIRTUALIZATION_EXCEPTION] =  { .Name = "Virtualization Exception",         .HasErrorCode = FALSE },
};

#pragma pack(push, 1)
typedef struct _IDT_DESCRIPTOR
{
    WORD    Limit;
    QWORD   Base;
}IDT_DESCRIPTOR;
#pragma pack(pop)

#pragma pack(push, 1)
#define IDT_TYPE_INTERRUPT_GATE (BIT(1) | BIT(2))
#define IDT_TYPE_TASK_GATE      (BIT(0) | BIT(2))
#define IDT_TYPE_TRAP_GATE      (BIT(0) | BIT(1) | BIT(2))
typedef struct _IDT_ENTRY
{
    WORD  Offset15_0;
    WORD  SegmentSelector;
    BYTE  IST;
    union
    {
        BYTE Type;
        BYTE Zero;
        BYTE Dpl;
        BYTE Present;
    }INFO;
    WORD  Offset31_16;
    DWORD Offset63_32;
    DWORD Reserved;
}IDT_ENTRY;
#pragma pack(pop)

#pragma pack(push, 1)
typedef IDT_ALIGN struct  _IDT_TABLE
{
    IDT_ENTRY Table[IDT_MAX_ENTRIES];
}IDT_TABLE;
#pragma pack(pop)

/* Just one IDT for all CPUs */
static BOOLEAN                  gIdtInitialized = FALSE;
static IDT_DESCRIPTOR           gIdtDescriptor;
static IDT_ALIGN IDT_TABLE      gIdtTable;

#pragma pack(push, 1)
struct _COMMON_REGISTER_STRUCTURE
{
    QWORD Cr3;
    QWORD Cr2;
    QWORD Cr0;
    QWORD Gs;
    QWORD Fs;
    QWORD Es;
    QWORD Ds;
    QWORD R15;
    QWORD R14;
    QWORD R13;
    QWORD R12;
    QWORD R11;
    QWORD R10;
    QWORD R9;
    QWORD R8;
    QWORD Rdi;
    QWORD Rsi;
    QWORD Rdx;
    QWORD Rcx;
    QWORD Rbx;
    QWORD Rbp;
    QWORD ExceptionVector;
    QWORD Rax;
};

struct _INTERRUPT_REGISTER_STUCTURE
{
    QWORD Rip;
    QWORD Cs;
    QWORD Rflags;
    QWORD Rsp;
    QWORD Ss;
};
#pragma pack(pop)

/* Main function to interrupt handler */
void InterruptHandler(COMMON_REGISTER_STRUCTURE* SavedRegisters);

/* Handlers from assembly */
extern void __Handler0();
extern void __Handler1();
#define GENERIC_HANDLER_SIZE    (((QWORD)&__Handler1) - ((QWORD)&__Handler0))
#define HANDLER0_ADDRESS        ((QWORD)&__Handler0)

/* Static functions */
static void _MakeDescriptor(BYTE InterruptNumber, QWORD FunctionAddress);
static void _DumpTrapFrame(COMMON_REGISTER_STRUCTURE* CommonRegisters, INTERRUPT_REGISTER_STUCTURE* InterruptRegisterStructure, QWORD ErrorCode);

/* Functions */
/*1*/
void
IdtInitialize(
    void
)
{
    // Initialize IDT Base & Limit
    gIdtDescriptor.Base = (QWORD)(&gIdtTable);
    gIdtDescriptor.Limit = sizeof(IDT_TABLE) - 1;

    for (DWORD vector = 0; vector < IDT_MAX_ENTRIES; vector++)
    {
        _MakeDescriptor((BYTE)vector, (QWORD)(HANDLER0_ADDRESS + (vector * GENERIC_HANDLER_SIZE)));
    }

    gIdtInitialized = TRUE;

    return;
}

void
InterruptHandler(
    COMMON_REGISTER_STRUCTURE* SavedRegisters
)
{
    QWORD errCode = gExceptionsInfo[SavedRegisters->ExceptionVector].HasErrorCode ?
        *((QWORD*)((QWORD)SavedRegisters + sizeof(COMMON_REGISTER_STRUCTURE))) :
        0;

    INTERRUPT_REGISTER_STUCTURE* intrreruptRegisterStructure = (INTERRUPT_REGISTER_STUCTURE*)
        ((QWORD)SavedRegisters
        + sizeof(COMMON_REGISTER_STRUCTURE)
        + (gExceptionsInfo[SavedRegisters->ExceptionVector].HasErrorCode ? sizeof(QWORD) : 0));

    if (SavedRegisters->ExceptionVector <= EXCEPTION_VIRTUALIZATION_EXCEPTION)
    {
        // Reserved intel exceptions
        _DumpTrapFrame(SavedRegisters, intrreruptRegisterStructure, errCode);
        __halt();
    }
    else if (SavedRegisters->ExceptionVector >= USER_EXCEPTIONS_FIRST_VECTOR)
    {
        // User-defined exception delivered.
        // Call the specific driver
        STATUS status = DriverCall(
            (BYTE)SavedRegisters->ExceptionVector,
            SavedRegisters,
            intrreruptRegisterStructure
        );
        if (SUCCESS(status))
        {
            PicSendEoi(PicGetIrqFromInterruptVector((BYTE)SavedRegisters->ExceptionVector));
        }
        else
        {
            // Driver fail, we disable it
            DriverDisable((BYTE)SavedRegisters->ExceptionVector);
        }
    }
    else
    {
        LOG_ERROR("Unnexpected exception delivered: [%d]\n", SavedRegisters->ExceptionVector);
        __halt();
    }

    return;
}

/*2*/
STATUS
IdtLoadForCurrentProcessor(
    void
)
{
    if (!gIdtInitialized) { return STATUS_IDT_NOT_INITIALIZED; }

    // Load the GDT
    __lidt(&gIdtDescriptor);

    return STATUS_SUCCESS;
}

static
void
_MakeDescriptor(
    BYTE InterruptNumber,
    QWORD FunctionAddress
)
{
    // Get exception entry from table
    IDT_ENTRY* idtEntry = &(gIdtTable.Table[InterruptNumber]);

    // Populate descriptor
    idtEntry->Offset15_0 = DWORD_LOW(QWORD_LOW(FunctionAddress));
    idtEntry->SegmentSelector = 0x28; // trust me
    idtEntry->INFO.Zero = 0;
    idtEntry->INFO.Type |= (0xE);
    idtEntry->INFO.Dpl |= BIT(5);
    idtEntry->INFO.Present |= BIT(7);
    idtEntry->IST = 0;
    idtEntry->Offset31_16 = DWORD_HIGH(QWORD_LOW(FunctionAddress));
    idtEntry->Offset63_32 = QWORD_HIGH(FunctionAddress);

    return;
}

static
void
_DumpTrapFrame(
    COMMON_REGISTER_STRUCTURE   *CommonRegisters,
    INTERRUPT_REGISTER_STUCTURE *InterruptRegisterStructure,
    QWORD                       ErrorCode
)
{
    BYTE exceptionVector = (BYTE)CommonRegisters->ExceptionVector;

    LOG("\n");
    LOG("[%s] Reserved Intel exception delivered: [%d] - [%s]\n", __func__, exceptionVector, gExceptionsInfo[exceptionVector].Name);
    if (gExceptionsInfo[exceptionVector].HasErrorCode) { LOG("[%s] Error code: [0x%x]\n", __func__, ErrorCode); }

    LOG("\n");
    LOG("[%s] RIP:      [0x%x]\n", __func__, InterruptRegisterStructure->Rip);
    LOG("[%s] CS:       [0x%x]\n", __func__, InterruptRegisterStructure->Cs);
    LOG("[%s] RFLAGS:   [0x%x]\n", __func__, InterruptRegisterStructure->Rflags);
    LOG("[%s] RSP:      [0x%x]\n", __func__, InterruptRegisterStructure->Rsp);
    LOG("[%s] SS:       [0x%x]\n", __func__, InterruptRegisterStructure->Ss);
    LOG("\n");
    LOG("[%s] CR3:      [0x%x]\n", __func__, CommonRegisters->Cr3);
    LOG("[%s] CS:       [0x%x]\n", __func__, CommonRegisters->Cr2);
    LOG("[%s] RFLAGS:   [0x%x]\n", __func__, CommonRegisters->Cr0);
    LOG("[%s] RSP:      [0x%x]\n", __func__, CommonRegisters->Gs);
    LOG("[%s] SS:       [0x%x]\n", __func__, CommonRegisters->Fs);
    LOG("[%s] RIP:      [0x%x]\n", __func__, CommonRegisters->Es);
    LOG("[%s] CS:       [0x%x]\n", __func__, CommonRegisters->Ds);
    LOG("[%s] RFLAGS:   [0x%x]\n", __func__, CommonRegisters->R15);
    LOG("[%s] RSP:      [0x%x]\n", __func__, CommonRegisters->R14);
    LOG("[%s] SS:       [0x%x]\n", __func__, CommonRegisters->R13);
    LOG("[%s] RIP:      [0x%x]\n", __func__, CommonRegisters->R12);
    LOG("[%s] CS:       [0x%x]\n", __func__, CommonRegisters->R11);
    LOG("[%s] RFLAGS:   [0x%x]\n", __func__, CommonRegisters->R10);
    LOG("[%s] RSP:      [0x%x]\n", __func__, CommonRegisters->R9);
    LOG("[%s] SS:       [0x%x]\n", __func__, CommonRegisters->R8);
    LOG("[%s] RIP:      [0x%x]\n", __func__, CommonRegisters->Rdi);
    LOG("[%s] CS:       [0x%x]\n", __func__, CommonRegisters->Rsi);
    LOG("[%s] RFLAGS:   [0x%x]\n", __func__, CommonRegisters->Rdx);
    LOG("[%s] RSP:      [0x%x]\n", __func__, CommonRegisters->Rcx);
    LOG("[%s] SS:       [0x%x]\n", __func__, CommonRegisters->Rbx);
    LOG("[%s] RSP:      [0x%x]\n", __func__, CommonRegisters->Rbp);
    LOG("[%s] SS:       [0x%x]\n", __func__, CommonRegisters->Rax);
    LOG("\n");

    return;
}
