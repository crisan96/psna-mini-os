#include "processor/paging.h"
#include "io/logging.h"
#include "memory/memory_manager.h"

#define MASK_PML4E_OFFSET(va)                       (((QWORD)(va)>>39)&0x1FF)
#define MASK_PDPTE_OFFSET(va)                       (((QWORD)(va)>>30)&0x1FF)
#define MASK_PDE_OFFSET(va)                         (((QWORD)(va)>>21)&0x1FF)
#define MASK_PTE_OFFSET(va)                         (((QWORD)(va)>>12)&0x1FF)
#define MASK_PAGE_OFFSET(va)                        ((QWORD)(va)&0xFFF)

#define PAGE_SHIFT                                  (12)

typedef struct _PAGING_GLOBAL_DATA
{
    SPINLOCK    Spinlock;   // not sure if this is a must yet, but for safety.. performance can be improved later :)
}PAGING_GLOBAL_DATA;
static PAGING_GLOBAL_DATA gPagingGlobalData;

#pragma warning(disable:4214)   // disable bit field types other than int warning
#pragma warning(disable:4201)   // disable nameless struct warning
typedef union _CR3_STRUCT_NO_PCID
{
    struct
    {
        QWORD   Ignored0    : BITFIELD(2, 0);
        QWORD   PWT         : BITFIELD(3, 3);
        QWORD   PCD         : BITFIELD(4, 4);
        QWORD   Ignored1    : BITFIELD(11, 5);
        QWORD   Pml4Addr    : BITFIELD(MAXPHYADDR - 1, 12);
        QWORD   Reserved    : BITFIELD(63, MAXPHYADDR); // Must be 0
    };
    QWORD Raw;
}CR3_STRUCT_NO_PCID;
static_assert(sizeof(CR3_STRUCT_NO_PCID) == sizeof(QWORD), "Invalid CR3_STRUCT_NO_PCID size.");

typedef union _PML4_ENTRY
{
    struct
    {
        QWORD           Present             : BITFIELD(0, 0);
        QWORD           ReadWrite           : BITFIELD(1, 1);
        QWORD           UserSupervisor      : BITFIELD(2, 2);
        QWORD           PWT                 : BITFIELD(3, 3);
        QWORD           PCD                 : BITFIELD(4, 4);
        QWORD           Accessed            : BITFIELD(5, 5);
        QWORD           Ignored0            : BITFIELD(6, 6);
        QWORD           Reserved            : BITFIELD(7, 7); // Must be 0
        QWORD           Ignored1            : BITFIELD(11, 8);
        QWORD           PdptPhysicalAddress : BITFIELD(MAXPHYADDR - 1, 12);
        QWORD           Ignored2            : BITFIELD(62, 52);
        QWORD           XD                  : BITFIELD(63, 63);
    };
    QWORD Raw;
} PML4_ENTRY;
static_assert(sizeof(PML4_ENTRY) == sizeof(QWORD), "Invalid PML4_ENTRY size.");

typedef struct _PML4
{
    PML4_ENTRY Pml4Entry[PAGE_SIZE / sizeof(PML4_ENTRY)];
}PML4;
static_assert(sizeof(PML4) == PAGE_SIZE, "Invalid PML4 size.");

typedef struct _PDPT_ENTRY
{
    QWORD           Present             : BITFIELD(0, 0);
    QWORD           ReadWrite           : BITFIELD(1, 1);
    QWORD           UserSupervisor      : BITFIELD(2, 2);
    QWORD           PWT                 : BITFIELD(3, 3);
    QWORD           PCD                 : BITFIELD(4, 4);
    QWORD           Accessed            : BITFIELD(5, 5);
    QWORD           Ignored0            : BITFIELD(6, 6);
    QWORD           PageSize            : BITFIELD(7, 7); // Must be 0
    QWORD           Ignored1            : BITFIELD(11, 8);
    QWORD           PdPhysicalAddress   : BITFIELD(MAXPHYADDR - 1, 12);
    QWORD           Ignored2            : BITFIELD(62, 52);
    QWORD           XD                  : BITFIELD(63, 63);
} PDPT_ENTRY;
static_assert(sizeof(PDPT_ENTRY) == sizeof(QWORD), "Invalid PDPT_ENTRY size.");

typedef struct _PDPT
{
    PDPT_ENTRY PdptEntry[PAGE_SIZE / sizeof(PDPT_ENTRY)];
}PDPT;
static_assert(sizeof(PDPT) == PAGE_SIZE, "Invalid PDPT size.");

typedef struct _PD_ENTRY
{
    QWORD           Present             : BITFIELD(0, 0);
    QWORD           ReadWrite           : BITFIELD(1, 1);
    QWORD           UserSupervisor      : BITFIELD(2, 2);
    QWORD           PWT                 : BITFIELD(3, 3);
    QWORD           PCD                 : BITFIELD(4, 4);
    QWORD           Accessed            : BITFIELD(5, 5);
    QWORD           Ignored0            : BITFIELD(6, 6);
    QWORD           PageSize            : BITFIELD(7, 7); // Must be 0
    QWORD           Ignored1            : BITFIELD(11, 8);
    QWORD           PtPhysicalAddress   : BITFIELD(MAXPHYADDR - 1, 12);
    QWORD           Ignored2            : BITFIELD(62, 52);
    QWORD           XD                  : BITFIELD(63, 63);
} PD_ENTRY;
static_assert(sizeof(PD_ENTRY) == sizeof(QWORD), "Invalid PD_ENTRY size.");

typedef struct _PD
{
    PD_ENTRY PdEntry[PAGE_SIZE / sizeof(PD_ENTRY)];
}PD;
static_assert(sizeof(PD) == PAGE_SIZE, "Invalid PD size.");

typedef struct _PT_ENTRY
{
    QWORD           Present             : BITFIELD(0, 0);
    QWORD           ReadWrite           : BITFIELD(1, 1);
    QWORD           UserSupervisor      : BITFIELD(2, 2);
    QWORD           PWT                 : BITFIELD(3, 3);
    QWORD           PCD                 : BITFIELD(4, 4);
    QWORD           Accessed            : BITFIELD(5, 5);
    QWORD           Dirty               : BITFIELD(6, 6);
    QWORD           PAT                 : BITFIELD(7, 7);
    QWORD           Global              : BITFIELD(8, 8);
    QWORD           Ignored0            : BITFIELD(11, 9);
    QWORD           PhysicalAddress     : BITFIELD(MAXPHYADDR - 1, 12);
    QWORD           Ignored1            : BITFIELD(62, 52);
    QWORD           XD                  : BITFIELD(63, 63);
} PT_ENTRY;
static_assert(sizeof(PT_ENTRY) == sizeof(QWORD), "Invalid PD_ENTRY size.");

typedef struct _PT
{
    PT_ENTRY PtEntry[PAGE_SIZE / sizeof(PT_ENTRY)];
}PT;
static_assert(sizeof(PT) == PAGE_SIZE, "Invalid PT size.");
#pragma warning(default:4201)   // enable nameless struct warning
#pragma warning(default:4214)   // enable bit field types other than int warning

/*1*/
STATUS
PagingInit(
    void
)
{
    STATUS status = STATUS_SUCCESS;

    status = SpinlockInit(&gPagingGlobalData.Spinlock);
    if (!SUCCESS(status))
    {
        return status;
    }

    return status;
}

/*2*/
STATUS
PagingMapKernelMemory(
    QWORD* Cr3
)
//
//  This function should init Cr3 with system addresses. With other words,
//  we start map the kernel binary starting from  KERNEL_MODE_BASE_VIRTUAL_MEM in this Cr3
//
{
    UNREFERENCED_PARAMETER(Cr3);
    return STATUS_NOT_IMPLEMENTED;
}

/*3*/
STATUS
PagingMapVaToPa(
    QWORD           *Cr3,
    BOOLEAN         CreateNewCr3,
    QWORD           Va,
    QWORD           Pa,
    ACCESS_RIGHT    AccessRights
)
{
    return PagingMapVaToPaEx(
        Cr3,
        CreateNewCr3,
        Va,
        Pa,
        CPU_MEMORY_TYPE_WRITEBACK,
        AccessRights
    );
}

/*4*/
STATUS
PagingMapVaToPaQuick(
    QWORD           Va,
    QWORD           Pa,
    ACCESS_RIGHT    AccessRights
)
{
    QWORD cr3 = __readcr3();

    return PagingMapVaToPa(
        &cr3,
        FALSE,
        Va,
        Pa,
        AccessRights
    );
}

/*5*/
STATUS
PagingMapVaToPaRange(
    QWORD           *Cr3,
    BOOLEAN         CreateNewCr3,
    QWORD           StartVa,
    QWORD           StartPa,
    QWORD           NumberOfPages,
    ACCESS_RIGHT    AccessRights
)
{
    STATUS status = STATUS_SUCCESS;

    if (NumberOfPages < 1) { return STATUS_INVALID_PARAMETER_5; }

    // Mapam separat prima pagina in caz ca trebuie
    // sa creem un nou cr3. La restul trebuie sa punem
    // FALSE la CreateNewCr3, pt ca e creat din prima iteratie
    status = PagingMapVaToPa(
        Cr3,
        CreateNewCr3,
        StartVa,
        StartPa,
        AccessRights
    );
    if (!SUCCESS(status)) { return status; }

    for (QWORD i = 1; i < NumberOfPages; ++i)
    {
        status = PagingMapVaToPa(
            Cr3,
            FALSE,
            StartVa + PAGE_SIZE * i,
            StartPa + PAGE_SIZE * i,
            AccessRights
        );
        if (!SUCCESS(status)) { return status; }
    }

    return status;
}

/*6*/
STATUS
PagingMapVaToPaRangeQuick(
    QWORD           StartVa,
    QWORD           StartPa,
    QWORD           NumberOfPages,
    ACCESS_RIGHT    AccessRights
)
{
    QWORD cr3 = __readcr3();

    return PagingMapVaToPaRange(
        &cr3,
        FALSE,
        StartVa,
        StartPa,
        NumberOfPages,
        AccessRights
    );
}

/*7*/ STATUS
PagingMapVaToPaEx(
    QWORD           *Cr3,
    BOOLEAN         CreateNewCr3,
    QWORD           Va,
    QWORD           Pa,
    CPU_MEMORY_TYPE MemoryType,
    ACCESS_RIGHT    AccessRights
)
{
    SpinlockAquire(&(gPagingGlobalData.Spinlock));

    STATUS status = STATUS_SUCCESS;

    if (!Cr3) { return STATUS_INVALID_PARAMETER_1; }

    // Offset in Va page should be equal with offset in Pa page.
    if (MASK_PAGE_OFFSET(Va) != MASK_PAGE_OFFSET(Pa)) { return STATUS_INVALID_PARAMETERS; }

    // Deocamdata suportam doar doua tipuri de memorie: WB si UC
    if ((MemoryType != CPU_MEMORY_TYPE_STRONG_UNCACHEABLE)
        && (MemoryType != CPU_MEMORY_TYPE_WRITEBACK)
        )
    {
        LOG_ERROR("We support only WC and UC memory type!\n");
        return STATUS_NOT_SUPPORTED;
    }

    QWORD alignedVa = Va & PAGE_MASK;
    QWORD alignedPa = Pa & PAGE_MASK;
    WORD pml4eIndex = MASK_PML4E_OFFSET(alignedVa);
    WORD pdpteIndex = MASK_PDPTE_OFFSET(alignedVa);
    WORD pdeIndex = MASK_PDE_OFFSET(alignedVa);
    WORD pteIndex = MASK_PTE_OFFSET(alignedVa);

    //
    // Get PML4 entry
    //
    QWORD pml4Va;
    PML4* pml4;
    PML4_ENTRY* pml4Entry;
    CR3_STRUCT_NO_PCID cr3;
    if (CreateNewCr3)
    {
        // Alloc a new pml4 entry and point cr3 to it
        QWORD pml4Pa;
        status = MemManagerAllocPageForPagingStructure(&pml4Pa);
        if (!SUCCESS(status)) { return status; }

        // For 4-level paging with CR4.PCIDE = 1, i = 0.
        // Otherwise, i = 2*PCD+PWT, where the PCD and PWT values come from CR3.
        // Noi vrem ca i sa fie 0, pentru ca ala e indexul in PAT pentru WB caching
        cr3.PCD = 0;
        cr3.PWT = 0;
        cr3.Reserved = 0; // must be 0
        cr3.Pml4Addr = pml4Pa >> PAGE_SHIFT;

        // Do not forget to save changes
        *Cr3 = cr3.Raw;
    }
    else
    {
        cr3.Raw = *Cr3;
    }
    status = MemManagerPaToVa(cr3.Pml4Addr << PAGE_SHIFT, &pml4Va);
    if (!SUCCESS(status)) { goto cleanup; }

    pml4 = (PML4*)pml4Va;
    pml4Entry = &(pml4->Pml4Entry[pml4eIndex]);

    //
    // Get PDPT entry
    //
    QWORD pdptVa;
    PDPT *pdpt;
    PDPT_ENTRY *pdptEntry;
    if (!pml4Entry->Present)
    {
        // Alloc a new PDPT and point pml4Entry to it
        QWORD pdptPa;
        status = MemManagerAllocPageForPagingStructure(&pdptPa);
        if (!SUCCESS(status)) { return status; }

        // Set pml4Entry to point to the new PDPT page
        // Note: MemManagerAllocPageForPagingStructure function already fill PDPT page with 0
        // so we just neet to set what we want
        pml4Entry->Present = TRUE;
        pml4Entry->ReadWrite = TRUE;
        pml4Entry->PdptPhysicalAddress = pdptPa >> PAGE_SHIFT;
    }
    status = MemManagerPaToVa(pml4Entry->PdptPhysicalAddress << PAGE_SHIFT, &pdptVa);
    if (!SUCCESS(status)) { goto cleanup; }

    pdpt = (PDPT*)pdptVa;
    pdptEntry = &(pdpt->PdptEntry[pdpteIndex]);

    //
    // Get PD entry
    //
    QWORD pdVa;
    PD *pd;
    PD_ENTRY *pdEntry;
    if (!pdptEntry->Present)
    {
        // Alloc a new PD and point pdptEntry to it
        QWORD pdPa;
        status = MemManagerAllocPageForPagingStructure(&pdPa);
        if (!SUCCESS(status)) { return status; }

        // Set pdptEntry to point to the new PD page
        // Note: MemManagerAllocPageForPagingStructure function already fill PDPT page with 0
        // so we just neet to set what we want
        pdptEntry->Present = TRUE;
        pdptEntry->ReadWrite = TRUE;
        pdptEntry->PdPhysicalAddress = pdPa >> PAGE_SHIFT;

    }
    status = MemManagerPaToVa(pdptEntry->PdPhysicalAddress << PAGE_SHIFT, &pdVa);
    if (!SUCCESS(status)) { goto cleanup; }

    pd = (PD*)pdVa;
    pdEntry = &(pd->PdEntry[pdeIndex]);

    //
    // Get PT entry
    //
    QWORD ptVa;
    PT* pt;
    PT_ENTRY* ptEntry;
    if (!pdEntry->Present)
    {
        // Alloc a new PT and point pdEntry to it
        QWORD ptPa;
        status = MemManagerAllocPageForPagingStructure(&ptPa);
        if (!SUCCESS(status)) { return status; }

        // Set pdEntry to point to the new PT page
        // Note: MemManagerAllocPageForPagingStructure function already fill PDPT page with 0
        // so we just neet to set what we want
        pdEntry->Present = TRUE;
        pdEntry->ReadWrite = TRUE;
        pdEntry->PtPhysicalAddress = ptPa >> PAGE_SHIFT;
    }
    status = MemManagerPaToVa(pdEntry->PtPhysicalAddress << PAGE_SHIFT, &ptVa);
    if (!SUCCESS(status)) { goto cleanup; }

    pt = (PT*)ptVa;
    ptEntry = &(pt->PtEntry[pteIndex]);
    if (ptEntry->Present)
    {
        // This Va is already mapped to a specific Pa
        status = STATUS_ADDRESS_ALREADY_MAPPED;
        goto cleanup;
    }

    //
    // Set caching
    //
    // For an access to the physical address that is the translation of a linear address,
    // i = 4*PAT+2*PCD+PWT, where the PAT, PCD, and PWT values come from the relevant PTE
    ptEntry->PAT = 0;   // Sper sa functioneze cu 0 aici, nu am vazut deocamdata undeva ca nu
    ptEntry->PCD = 0;   //
    ptEntry->PWT = CPU_MEMORY_TYPE_WRITEBACK == MemoryType ? WRITE_BACK_MEMORY_TYPE_INDEX_IN_PAT : UNCACHEABLE_MEOMRY_INDEX_IN_PAT;;

    //
    // Set physical address
    //
    ptEntry->PhysicalAddress = alignedPa >> PAGE_SHIFT;

    //
    // Set access rights
    //
    ptEntry->Present = TRUE;
    ptEntry->ReadWrite = (AccessRights & ACCESS_RIGHT_WRITE) ? TRUE : FALSE;
    ptEntry->UserSupervisor = FALSE; // Deocamdata nu avem suport pentru maparea User-Mode
    ptEntry->XD = (AccessRights & ACCES_RIGHT_EXECUTE) ? FALSE : TRUE;

    // Invalidate TLB for this virtual address,
    // but just if we map for current CR3 :D
    if (*Cr3 == __readcr3()) { __invlpg((void*)alignedVa); }

cleanup:
    if (!SUCCESS(status))
    {
        // Clean
        //....
    }

    SpinlockRelease(&(gPagingGlobalData.Spinlock));
    return status;
}

/*8*/
STATUS
PagingUnmapAddress(
    QWORD Va
)
{
    QWORD cr3 = __readcr3();

    return PagingUnmapAddressEx(cr3, Va);
}

/*9*/
STATUS
PagingUnmapAddressRange(
    QWORD Va,
    QWORD NumberOfPages
)
{
    for (QWORD i = 0; i < NumberOfPages; ++i)
    {
        STATUS status = PagingUnmapAddress(Va + i * PAGE_SIZE);
        if (!SUCCESS(status)) { return status; }
    }

    return STATUS_SUCCESS;
}

STATUS
PagingUnmapAddressEx(
    QWORD Cr3,
    QWORD Va
)
{
    SpinlockAquire(&(gPagingGlobalData.Spinlock));

    if (!Cr3) { return STATUS_INVALID_PARAMETER_1; }

    QWORD alignedVa = Va & PAGE_MASK;
    WORD pml4eIndex = MASK_PML4E_OFFSET(alignedVa);
    WORD pdpteIndex = MASK_PDPTE_OFFSET(alignedVa);
    WORD pdeIndex = MASK_PDE_OFFSET(alignedVa);
    WORD pteIndex = MASK_PTE_OFFSET(alignedVa);

    //
    // Get PML4 entry
    //
    QWORD pml4Va;
    PML4* pml4;
    PML4_ENTRY* pml4Entry;
    CR3_STRUCT_NO_PCID cr3;

    cr3.Raw = Cr3;

    STATUS status = MemManagerPaToVa(cr3.Pml4Addr << PAGE_SHIFT, &pml4Va);
    if (!SUCCESS(status)) { return status; }

    pml4 = (PML4*)pml4Va;
    pml4Entry = &(pml4->Pml4Entry[pml4eIndex]);

    //
    // Get PDPT entry
    //
    QWORD pdptVa;
    PDPT *pdpt;
    PDPT_ENTRY *pdptEntry;
    if (!pml4Entry->Present)
    {
        return STATUS_SUCCESS;
    }
    status = MemManagerPaToVa(pml4Entry->PdptPhysicalAddress << PAGE_SHIFT, &pdptVa);
    if (!SUCCESS(status)) { return status; }

    pdpt = (PDPT*)pdptVa;
    pdptEntry = &(pdpt->PdptEntry[pdpteIndex]);

    //
    // Get PD entry
    //
    QWORD pdVa;
    PD *pd;
    PD_ENTRY *pdEntry;
    if (!pdptEntry->Present)
    {
        return STATUS_SUCCESS;
    }
    status = MemManagerPaToVa(pdptEntry->PdPhysicalAddress << PAGE_SHIFT, &pdVa);
    if (!SUCCESS(status)) { return status; }

    pd = (PD*)pdVa;
    pdEntry = &(pd->PdEntry[pdeIndex]);

    //
    // Get PT entry
    //
    QWORD ptVa;
    PT* pt;
    PT_ENTRY* ptEntry;
    if (!pdEntry->Present)
    {
        return STATUS_SUCCESS;
    }
    status = MemManagerPaToVa(pdEntry->PtPhysicalAddress << PAGE_SHIFT, &ptVa);
    if (!SUCCESS(status)) { return status; }

    pt = (PT*)ptVa;
    ptEntry = &(pt->PtEntry[pteIndex]);

    // Set this entry to not present
    ptEntry->Present = FALSE;

    // Invalidate TLB for this virtual address,
    // but just if we map for current CR3 :D
    if (Cr3 == __readcr3()) { __invlpg((void*)alignedVa); }

    SpinlockRelease(&(gPagingGlobalData.Spinlock));

    return STATUS_SUCCESS;
}