#include "apps/command_line.h"
#include "io/vga.h"
#include "io/logging.h"
#include "driver/keyboard.h"
#include "processor/cpu.h"

#define CMD_LINE_TEXT               "Enter your command (type help for commands list): "
#define CMD_LINE_TEXT_X_COORD       10
#define CMD_LINE_TEXT_Y_COORD       0
#define USER_ANSWER_X_COORD         CMD_LINE_TEXT_X_COORD
#define USER_ANSWER_Y_COORD         (CMD_LINE_TEXT_Y_COORD + sizeof(CMD_LINE_TEXT))
#define CMD_LINE_RESPONSE_X_COORD   (CMD_LINE_TEXT_X_COORD + 1)
#define CMD_LINE_RESPONSE_Y_COORD   0

#define COMMAND_HELP                "help"
#define COMMAND_RESET               "reset"

typedef struct _CMD_LINE_GLOBAL_DATA
{
#define MAX_CMD_LINE_LENGTH     (VGA_WIDTH - USER_ANSWER_Y_COORD)
#define MAX_RESPONSE_LENGTH     (VGA_HEIGHT * VGA_WIDTH - (CMD_LINE_RESPONSE_X_COORD * VGA_WIDTH + CMD_LINE_RESPONSE_Y_COORD))
    BYTE    CmdLineNextIndex;
    CHAR    CmdLine[MAX_CMD_LINE_LENGTH];
    CHAR    Response[MAX_RESPONSE_LENGTH];
}CMD_LINE_GLOBAL_DATA;
static CMD_LINE_GLOBAL_DATA gCmdLineData;

/* Function which will be called every time a key is pressed */
static void _KeyWasPressed(void);

static void _ExecuteCommand(void);
static void _HelpCommand(void);
static void _UnknownCommand(void);
static void __forceinline _ClearLastCommandAnswer(void);

/*1*/
STATUS
CmdLineInit(
    void
)
{
    crt_memset((void*)&gCmdLineData, 0, sizeof(gCmdLineData));

    // Register the callback function which will be called every time a key is pressed
    STATUS status = KeyboardDriverRegisterCallback(_KeyWasPressed, FALSE, 0);
    if (!SUCCESS(status)) { return status; }

    VgaPrintString(
        CMD_LINE_TEXT_X_COORD,
        CMD_LINE_TEXT_Y_COORD,
        CMD_LINE_TEXT,
        sizeof(CMD_LINE_TEXT),
        VgaGetBackgroundColor(),
        VGA_COLOR_BLACK
    );

    return STATUS_NOT_IMPLEMENTED;
}

static
void
_KeyWasPressed(
    void
)
{
    CHAR lastPressedKey = KeyboardDriverGetLastKeyPressed();
    if (lastPressedKey == KEY_ENTER)
    {
        _ExecuteCommand();

        // After the command was executed, we clear the user input
        gCmdLineData.CmdLineNextIndex = 0;
        crt_memset((void*)gCmdLineData.CmdLine, 0, sizeof(gCmdLineData.CmdLine));
    }
    else if (lastPressedKey == KEY_BACKSPACE)
    {
        // Delete last key, if there is at least one key
        if (gCmdLineData.CmdLineNextIndex > 0)
        {
            --gCmdLineData.CmdLineNextIndex;
            gCmdLineData.CmdLine[gCmdLineData.CmdLineNextIndex] = 0;
        }
    }
    else
    {
        // Regulat key was pressed. Save it
        gCmdLineData.CmdLine[gCmdLineData.CmdLineNextIndex++] = KeyboardDriverGetLastKeyPressed();
    }

    // Print the command line input from user
    VgaPrintString(
        USER_ANSWER_X_COORD,
        USER_ANSWER_Y_COORD,
        gCmdLineData.CmdLine,
        sizeof(gCmdLineData.CmdLine),
        VgaGetBackgroundColor(),
        VGA_COLOR_BLACK
    );

    // Print response (could be blank)
    VgaPrintString(
        CMD_LINE_RESPONSE_X_COORD,
        CMD_LINE_RESPONSE_Y_COORD,
        gCmdLineData.Response,
        sizeof(gCmdLineData.Response),
        VgaGetBackgroundColor(),
        VGA_COLOR_BLACK
    );

    return;
}

static
void
_ExecuteCommand(
    void
)
{
    _ClearLastCommandAnswer();

    if (crt_strcmp(gCmdLineData.CmdLine, COMMAND_HELP) == 0)
    {
        _HelpCommand();
    }
    else if (crt_strcmp(gCmdLineData.CmdLine, COMMAND_RESET) == 0)
    {
        CpuReset();
    }
    else
    {
        _UnknownCommand();
    }

    return;
}

static
void
_HelpCommand(
    void
)
{
    static char helpMessage[] =
        "reset - Reset computer.";
    static_assert(sizeof(helpMessage) <= MAX_RESPONSE_LENGTH, "Invalid sizeof(helpMessage).");

    crt_memcpy((void*)gCmdLineData.Response, (void*)helpMessage, sizeof(helpMessage));

    return;
}

static
void
_UnknownCommand(
    void
)
{
    crt_memcpy((void*)gCmdLineData.Response, "Unknown command!", sizeof("Unknown command!"));
    return;
}

static
void
__forceinline
_ClearLastCommandAnswer(
    void
)
{
    crt_memset((void*)gCmdLineData.Response, 0, sizeof(gCmdLineData.Response));
    return;
}