#include "sync/spinlock.h"

/* Functions from ASM code */
extern void __SpinlockAquire(SPINLOCK* Spinlock);
extern void __SpinlockRelease(SPINLOCK* Spinlock);

/* Implementations */
/*1*/
STATUS
SpinlockInit(
    SPINLOCK* Spinlock
)
{
    if (!Spinlock) { return STATUS_INVALID_PARAMETER_1; }

    Spinlock->Lock = 0;

    return STATUS_SUCCESS;
}

/*2*/
STATUS
SpinlockAquire(
    SPINLOCK* Spinlock
)
{
    if (!Spinlock) { return STATUS_INVALID_PARAMETER_1; }

    __SpinlockAquire(Spinlock);

    return STATUS_SUCCESS;
}

/*3*/
STATUS
SpinlockRelease(
    SPINLOCK* Spinlock
)
{
    if (!Spinlock) { return STATUS_INVALID_PARAMETER_1; }

    __SpinlockRelease(Spinlock);

    return STATUS_SUCCESS;
}