%define LOCK_USED   1
%define LOCK_FREE   0

; Declare functions as global so C code can use them
global __SpinlockAquire
global __SpinlockRelease

[bits 64]
; void __SpinlockAquire(SPINLOCK* Spinlock);
__SpinlockAquire:
    jmp .acquire_lock

    .spin_with_pause:
    pause                               ; Tell CPU we're spinning
    test        dword [RCX], LOCK_USED  ; Is the lock free?
    jnz         .spin_with_pause        ; No, wait

    .acquire_lock:
    lock bts    DWORD [RCX], LOCK_FREE  ; Attempt to acquire the lock (in case lock is uncontended)
    jc          .spin_with_pause

    ret

[bits 64]
; void __SpinlockRelease(SPINLOCK* Spinlock);
__SpinlockRelease:
    mov     DWORD [RCX], LOCK_FREE
    ret