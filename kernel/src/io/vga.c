#include "io/vga.h"
#include "lib/crt.h"

static char MiniOsLogo[] =
"\
__  __   __   __   __   __          _____   _________  \n\
| | | |  | |  | \\  | |  | |  ___   /  __ \\  |  ______| \n\
| \\/  |  | |  | |\\ | |  | | |___| |  |  | | | |______  \n\
| | | |  | |  | \\_\\| |  | |       |  |__| | |______  | \n\
|_| | |  |_|  |_|  |_|  |_|        \\_____/  _________| \n\
\n\n";

/* Video memory address */
#define VGA_BASE_ADDRESS        0xB8000ULL

#pragma pack(push, 1)
#pragma warning(disable:4214)   // disable bit field types other than int warning
#pragma warning(disable:4201)   // disable nameless struct warning
typedef union _VGA_MATRIX_ENTRY
{
    struct
    {
        WORD    CharacterAsciiCode  : BITFIELD(7, 0);
        //WORD    Reserved            : BITFIELD(8, 8); // From osdev: The attribute byte carries the foreground colour in its lowest 4 bits
                                                        // and the background color in its highest 3 bits.
                                                        // The interpretation of bit #7 depends on how you (or the BIOS) configured the hardware
                                                        // (see VGA Resources for additional info).
                                                        // Pare ca daca chiar il folosesc ca si reserved nu merge (si era cumva evident ca raman doar
                                                        // 3 biti pentru background .. )
        WORD    ForegroundColor     : BITFIELD(11, 8);
        WORD    BackgroundColor     : BITFIELD(15, 12);
    };

    WORD Raw;
}VGA_MATRIX_ENTRY;
static_assert(sizeof(VGA_MATRIX_ENTRY) == sizeof(WORD), "Invalid VGA_MATRIX_ENTRY size.");
#pragma warning(default:4201)   // enable nameless struct warning
#pragma warning(default:4214)   // enable bit field types other than int warning
#pragma pack(pop)

typedef struct _VGA_GLOBAL_DATA
{
    BOOLEAN             IsInitialized;
    VGA_STANDARD_COLOR  BackgroundColor;
    VGA_MATRIX_ENTRY**  VgaMatrix;
}VGA_GLOBAL_DATA;
static VGA_GLOBAL_DATA gVgaGlobalData;

/* Static functions */
static void _VgaClear(VGA_STANDARD_COLOR BackgroundColor);
static void _VgaPrintChar(BYTE CoordX, BYTE CoordY, CHAR Character, VGA_STANDARD_COLOR BackgroundColor, VGA_STANDARD_COLOR ForegroundColor);
static void _VgaPrintString(BYTE StartCoordX, BYTE StartCoordY, CHAR* String, DWORD StringSize, VGA_STANDARD_COLOR BackgroundColor, VGA_STANDARD_COLOR ForegroundColor);
static void _VgaSetBackgroundColor(VGA_STANDARD_COLOR BackgroundColor);

/* Implementations */
/*1*/
STATUS
VgaInit(
    void
)
{
    crt_memset((void*)&gVgaGlobalData, 0, sizeof(gVgaGlobalData));

    for (BYTE i = 0; i < VGA_HEIGHT; i++)
    {
        gVgaGlobalData.VgaMatrix[i] = (VGA_MATRIX_ENTRY*)(VGA_BASE_ADDRESS + VGA_WIDTH * i * sizeof(VGA_MATRIX_ENTRY));
    }

    gVgaGlobalData.BackgroundColor = VGA_COLOR_BLUE;

    _VgaClear(gVgaGlobalData.BackgroundColor);

    _VgaPrintString(0, 0, MiniOsLogo, sizeof(MiniOsLogo), gVgaGlobalData.BackgroundColor, VGA_COLOR_LIGHT_BLUE);

    gVgaGlobalData.IsInitialized = TRUE;

    return STATUS_SUCCESS;
}

/*2*/
STATUS
VgaClear(
    VGA_STANDARD_COLOR BackgroundColor
)
{
    if (!gVgaGlobalData.IsInitialized) { return STATUS_COMPONENT_NOT_INITIALIZED; }

    _VgaClear(BackgroundColor);

    return STATUS_SUCCESS;
}

/*3*/
STATUS
VgaPrintChar(
    BYTE                CoordX,
    BYTE                CoordY,
    CHAR                Character,
    VGA_STANDARD_COLOR  BackgroundColor,
    VGA_STANDARD_COLOR  ForegroundColor
)
{
    if (!gVgaGlobalData.IsInitialized) { return STATUS_COMPONENT_NOT_INITIALIZED; }

    if (CoordX >= VGA_HEIGHT) { return STATUS_INVALID_PARAMETER_1; }

    if (CoordY >= VGA_WIDTH) { return STATUS_INVALID_PARAMETER_2; }

    _VgaPrintChar(CoordX, CoordY, Character, BackgroundColor, ForegroundColor);

    return STATUS_SUCCESS;
}

/*4*/
STATUS
VgaPrintString(
    BYTE                StartCoordX,
    BYTE                StartCoordY,
    CHAR                *String,
    DWORD               StringSize,
    VGA_STANDARD_COLOR  BackgroundColor,
    VGA_STANDARD_COLOR  ForegroundColor
)
{
    if (!gVgaGlobalData.IsInitialized) { return STATUS_COMPONENT_NOT_INITIALIZED; }

    if (StartCoordX >= VGA_HEIGHT) { return STATUS_INVALID_PARAMETER_1; }

    if (StartCoordY >= VGA_WIDTH) { return STATUS_INVALID_PARAMETER_2; }

    if (!String) { return STATUS_INVALID_PARAMETER_3; }

    if (StringSize > (VGA_HEIGHT* VGA_WIDTH - (StartCoordX * VGA_WIDTH + StartCoordY))) { return STATUS_INVALID_PARAMETER_4; }

    _VgaPrintString(StartCoordX, StartCoordY, String, StringSize, BackgroundColor, ForegroundColor);

    return STATUS_SUCCESS;
}

/*5*/
STATUS
VgaSetBackgroundColor(
    VGA_STANDARD_COLOR BackgroundColor
)
{
    if (!gVgaGlobalData.IsInitialized) { return STATUS_COMPONENT_NOT_INITIALIZED; }

    _VgaSetBackgroundColor(BackgroundColor);

    return STATUS_SUCCESS;
}

/*6*/
VGA_STANDARD_COLOR
VgaGetBackgroundColor(
    void
)
{
    if (!gVgaGlobalData.IsInitialized) { return VGA_COLOR_END; }

    return gVgaGlobalData.BackgroundColor;
}

/* Static functions */
static
void
_VgaClear(
    VGA_STANDARD_COLOR BackgroundColor
)
{
    for (BYTE i = 0; i < VGA_HEIGHT; ++i)
    {
        for (BYTE j = 0; j < VGA_WIDTH; ++j)
        {
            gVgaGlobalData.VgaMatrix[i][j].CharacterAsciiCode = 0;
            gVgaGlobalData.VgaMatrix[i][j].ForegroundColor = BackgroundColor;
            gVgaGlobalData.VgaMatrix[i][j].BackgroundColor = BackgroundColor;
        }
    }

    return;
}

static
void
_VgaPrintChar(
    BYTE                CoordX,
    BYTE                CoordY,
    CHAR                Character,
    VGA_STANDARD_COLOR  BackgroundColor,
    VGA_STANDARD_COLOR  ForegroundColor
)
{
    gVgaGlobalData.VgaMatrix[CoordX][CoordY].CharacterAsciiCode = Character;
    gVgaGlobalData.VgaMatrix[CoordX][CoordY].ForegroundColor = ForegroundColor;
    gVgaGlobalData.VgaMatrix[CoordX][CoordY].BackgroundColor = BackgroundColor;

    return;
}

static
void
_VgaPrintString(
    BYTE                StartCoordX,
    BYTE                StartCoordY,
    CHAR                *String,
    DWORD               StringSize,
    VGA_STANDARD_COLOR  BackgroundColor,
    VGA_STANDARD_COLOR  ForegroundColor
)
{
    DWORD indexInString = 0;
    while (indexInString < StringSize)
    {
        if (
            (String[indexInString] == '\n')
            || (StartCoordY == VGA_WIDTH)
            )
        {
            ++StartCoordX;
            StartCoordY = 0;
            ++indexInString;
            continue;
        }

        _VgaPrintChar(StartCoordX, StartCoordY, String[indexInString], BackgroundColor, ForegroundColor);
        ++StartCoordY;
        ++indexInString;
    }


    return;
}

static
void
_VgaSetBackgroundColor(
    VGA_STANDARD_COLOR BackgroundColor
)
{
    gVgaGlobalData.BackgroundColor = BackgroundColor;

    for (BYTE i = 0; i < VGA_HEIGHT; ++i)
    {
        for (BYTE j = 0; j < VGA_WIDTH; ++j)
        {
            gVgaGlobalData.VgaMatrix[i][j].BackgroundColor = BackgroundColor;
        }
    }

    return;
}
