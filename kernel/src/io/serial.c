#include "io/serial.h"
#include "lib/intrin.h"

typedef struct _SERIAL_GLOBAL_VARIABLES
{
    WORD    SerialPort;    // Save serial address
    BOOLEAN Initialized;
}SERIAL_GLOBAL_VARIABLES;

/* Serial global variables */
static SERIAL_GLOBAL_VARIABLES gSerialVariables = { .SerialPort = 0, .Initialized = FALSE };

/* Useful defines */                                            /*********************************************************************************************/
#define MAX_NO_OF_SERIAL_PORTS              4                   /*   Address(size)     |                         Description                                 */
#define BIOS_AREA_SERIAL_PORTS_ADDRESS      ((WORD*)0x400)      /*  0x0400(4 words)    | IO ports for COM1-COM4 serial(each address is 1 word, zero if none) */
                                                                /*********************************************************************************************/
#define INT_ENABLE_REG_OFFSET               0x01
#define BAUD_RATE_DIV_LSB_OFFSET            0x00
#define BAUD_RATE_DIV_MSB_OFFSET            0x01
#define LINE_CONTROL_REG_OFFSET             0x03
#define FIFO_CONTROL_REG_OFFSET             0x02
#define LINE_STATUS_REG_OFFSET              0x05

#define DISABLE_INTERRUPTS                  0x00
#define DLAB_ENABLE                         0x80
#define LINE_PROTOCOL_8N1                   0x03
#define ENABLE_FIFO                         0xC7
#define TRANSMIT_EMPTY                      0x60

/* Static functions */
__forceinline static void SerialOut(BYTE Data);

/* Implementations */
/*1*/
STATUS
SerialInit(
    void
)
{
    gSerialVariables.Initialized = 0;

    // Find firs available serial com port
    for (BYTE i = 0; i < MAX_NO_OF_SERIAL_PORTS; ++i)
    {
        gSerialVariables.SerialPort = BIOS_AREA_SERIAL_PORTS_ADDRESS[i];
        if (gSerialVariables.SerialPort != 0)
        {
            break;
        }
    }

    if (gSerialVariables.SerialPort == 0)
    {
        return STATUS_NO_SERIAL_PORT_AVAILABLE;
    }

    /* Initialization */
    gSerialVariables.Initialized = TRUE;
    __outbyte(gSerialVariables.SerialPort + INT_ENABLE_REG_OFFSET, DISABLE_INTERRUPTS);     // Disable all interrupts
    __outbyte(gSerialVariables.SerialPort + LINE_CONTROL_REG_OFFSET, DLAB_ENABLE);          // Enable DLAB (set baud rate divisor)
    __outbyte(gSerialVariables.SerialPort + BAUD_RATE_DIV_LSB_OFFSET, 1);                   // Set divisor to 1 (lo byte) 115200 baud
    __outbyte(gSerialVariables.SerialPort + BAUD_RATE_DIV_MSB_OFFSET, 0);                   //                  (hi byte)
    __outbyte(gSerialVariables.SerialPort + LINE_CONTROL_REG_OFFSET, LINE_PROTOCOL_8N1);    // 8 bits, no parity, one stop bit
    __outbyte(gSerialVariables.SerialPort + FIFO_CONTROL_REG_OFFSET, ENABLE_FIFO);          // Enable FIFO, clear them, with 14-byte threshold

    return STATUS_SUCCESS;
}

/*2*/
STATUS
SerialWriteByte(
    BYTE Data
)
{
    if (!gSerialVariables.Initialized)
    {
        return STATUS_SERIAL_NOT_INITIALIZED;
    }

    SerialOut(Data);

    return STATUS_SUCCESS;
}

/*3*/
STATUS
SerialWriteBuffer(
    CHAR* Buffer
)
{
    DWORD i = 0;

    if (!gSerialVariables.Initialized)
    {
        return STATUS_SERIAL_NOT_INITIALIZED;
    }

    while (Buffer[i] != '\0')
    {
        SerialOut(Buffer[i]);
        ++i;
    }

    return STATUS_SUCCESS;
}

/*4*/
STATUS
SerialWriteNBuffer(
    CHAR* Buffer,
    DWORD Length
)
{
    DWORD i = 0;

    if (!gSerialVariables.Initialized)
    {
        return STATUS_SERIAL_NOT_INITIALIZED;
    }

    while (i < Length)
    {
        SerialOut(Buffer[i]);
        ++i;
    }

    return STATUS_SUCCESS;
}

__forceinline
static void
SerialOut(
    BYTE Data
)
{
    // Check the state of the LSR.Empty Transmitter Holding Register (bit 5) or LSR.Empty Data Holding Registers (bit 6) from LSR (base + 5)
    while ((__inbyte(gSerialVariables.SerialPort + LINE_STATUS_REG_OFFSET) & TRANSMIT_EMPTY) == 0);

    // Write on serial
    __outbyte(gSerialVariables.SerialPort, Data);

    return;
}