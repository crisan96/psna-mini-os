#include "lib/std.h"
#include "io/vga.h"
#include "global/global.h"
#include "processor/cpu.h"
#include "io/logging.h"
#include "processor/idt.h"
#include "devices/pic.h"
#include "driver/timer.h"
#include "driver/keyboard.h"
#include "memory/memory_manager.h"
#include "driver/driver.h"
#include "apps/command_line.h"

void KernelMain(ASM_DATA* AsmData)
{
    STATUS status = CpuSetRegistersForCurrentCpu();
    if (!SUCCESS(status)) { __halt(); }

    status = VgaInit();
    if (!SUCCESS(status)) { __halt(); }

    status = KernelGlobalDataInit(AsmData);
    if (!SUCCESS(status))
    {
        VgaPrintString(0, 0, "KernelGlobalDataInit failed!", sizeof("KernelGlobalDataInit failed!"), VGA_COLOR_CYAN, VGA_COLOR_RED);
        __halt();
    }

    /* After global data is successfully initalized,
    we can initialize serial output */
    status = SerialInit();
    if (!SUCCESS(status))
    {
        VgaPrintString(0, 0, "SerialInit failed!", sizeof("SerialInit failed!"), VGA_COLOR_CYAN, VGA_COLOR_RED);
        __halt();
    }

    /* Dump memory map from BIOS int 0x15 */
    MmapDump();

    {/* Enable interrupts */
        LOG_INFO("About to initialize idt table and load for current processor (BSP)\n");
        IdtInitialize();
        status = IdtLoadForCurrentProcessor();
        if (!SUCCESS(status))
        {
            LOG_ERROR("IdtLoadForCurrentProcessor failed with status = 0x%x\n", status);
            __halt();
        }

        LOG_INFO("About to initialize PIC with base vector for PIC1 = [0x%x] and base vector for PIC2 = [0x%x]\n", PIC1_BASE_VECTOR, PIC2_BASE_VECTOR);;
        status = PicInit(PIC1_BASE_VECTOR, PIC2_BASE_VECTOR);
        if (!SUCCESS(status))
        {
            LOG_ERROR("PicInit failed with status = 0x%x\n", status);
            __halt();
        }

        status = DriverInit();
        if (!SUCCESS(status))
        {
            LOG_ERROR("DriverInit failed with status = 0x%x\n", status);
            __halt();
        }
        LOG_INFO("OS ready to register drivers...\n");

        status = TimerDriverInit();
        if (!SUCCESS(status))
        {
            LOG_ERROR("TimerDriverInit failed with status = 0x%x\n", status);
            __halt();
        }
        LOG_INFO("Successfully registered timer driver!\n");

        status = KeyboardDriverInit();
        if (!SUCCESS(status))
        {
            LOG_ERROR("KeyboardDriverInit failed with status = 0x%x\n", status);
            __halt();
        }
        LOG_INFO("Successfully registered keyboard driver!\n");

        _enable();
    }

    /* Initialize memory manager */
    status = MemManagerInit();
    if (!SUCCESS(status))
    {
        LOG_ERROR("MemManagerInit failed with status = 0x%x\n", status);
        __halt();
    }
    LOG_INFO("Successfully initialized memory manager!\n");

    /* Init command line so the user can communicate with the OS */
    status = CmdLineInit();
    if (!SUCCESS(status))
    {
        LOG_ERROR("CmdLineInit failed with status = 0x%x\n", status);
        __halt();
    }

    while(1);


    // TODO!!! PIC programming; see http://www.osdever.net/tutorials/view/programming-the-pic
    // TODO!!! define interrupt routines and dump trap frame

    // TODO!!! Timer programming

    // TODO!!! Keyboard programming

    // TODO!!! Implement a simple console

    // TODO!!! read disk sectors using PIO mode ATA

    // TODO!!! Memory management: virtual, physical and heap memory allocators
}
