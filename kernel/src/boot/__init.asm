%include "../../include/lib/__defs.asm"
extern KernelMain
global ASMEntryPoint

;
; PXE 32 bit loader code
;
; for documentation check out MULTIBOOT 0.6.96 specs
; http://www.gnu.org/software/grub/manual/multiboot/multiboot.html
;

SECTION .boot
;
; multiboot starts in 32 bit PROTECTED MODE, without paging beeing enabled (FLAT)
; check out '3.2 Machine state' from docs
;

;;
;; IMPORTANT: this should be the first stuff to be linked into this segment
;; check out '3.1.1 The layout of Multiboot header'
;;
multiboot_header: istruc MULTIBOOT_HEADER
    at MULTIBOOT_HEADER.magic,          dd MULTIBOOT_HEADER_MAGIC
    at MULTIBOOT_HEADER.flags,          dd MULTIBOOT_HEADER_FLAGS
    at MULTIBOOT_HEADER.checksum,       dd 0-(MULTIBOOT_HEADER_MAGIC + MULTIBOOT_HEADER_FLAGS)
    at MULTIBOOT_HEADER.header_addr,    dd IMAGE_BASE_PA + PE_HEADERS_SIZE
    at MULTIBOOT_HEADER.load_addr,      dd IMAGE_BASE_PA
    at MULTIBOOT_HEADER.load_end_addr,  dd 0
    at MULTIBOOT_HEADER.bss_end_addr,   dd 0
    at MULTIBOOT_HEADER.entry_addr,     dd VA32(ASMEntryPoint)
    at MULTIBOOT_HEADER.mode_type,      dd 0
    at MULTIBOOT_HEADER.width,          dd 0
    at MULTIBOOT_HEADER.height,         dd 0
    at MULTIBOOT_HEADER.depth,          dd 0
iend

; to avoid calculation errors, offer a simple & reusable method for obtaining a selector given a descriptor's name
%define SEL(x)          ((bootGdt. %+ x) - bootGdt.tableStart)

[BITS 32]
ASMEntryPoint:
    ;
    ; Disable interrupts, setup a basic stack and perform MULTIBOOT sanity checks
    ;
    cli
    cld
    PING(0, 0, '1010')                       ; signal our presence by writing some VGA memory location(s)

    ; Check if multiboot loaded us
    cmp     eax, MULTIBOOT_LOADER_MAGIC
    jne     .error

    mov     esp, PROTECTED_MODE_TOP_OF_STACK ; just below the kernel

    ; Save current state
    ; We don't have to save current GDT as we will load our own GDT
    ; IDT
    sidt    [PA(reloc.savedIdt)]

    ; Copy real mode code at 0x8000, witch is smaller than 1MB
    mov     esi, PA(reloc)
    mov     edi, REAL_MODE_BASE
    mov     ecx, (relocEnd - reloc)
    cld
    rep     movsb

    ; Load our own GDT for 16 bits
    lgdt    [VA32(bootGdt)]
    mov     ax, SEL(data16)
    mov     ds, ax
    mov     es, ax
    mov     ss, ax
    mov     fs, ax
    mov     gs, ax

    ;; Far jump to 16-bit protected mode
    jmp     SEL(code16):REAL_MODE_BASE

.restorePointFrom16:
    ; Here we have a 32 bit GDT
    [bits 32]
    ; Restore miss stack at it's initial value
    MOV     ESP, REAL_MODE_TOP_OF_STACK ; just below the kernel
    ; Save mmap from INT15
    ; Number of entries
    mov     edi, PA(gAsmData)                   ; Where we will save (observe that mmap is first entry in this struct)
    add     edi, 0x8                            ; sizeof(QWORD) => skip NextPagingStructureEntry
    mov     esi, REBASE(reloc.entries)          ; PA where mmap is stored now (< 1MB)
    mov     ecx, 4                              ; sizeof(DWORD)
    cld
    rep     movsb
    ;; Entries
    mov     ecx, DWORD  [REBASE(reloc.entries)] ; ecx = number of entries
    mov     eax, SMAP_ENTRY_SIZE                ; eax = size of an entry
    xor     edx, edx
    mul     ecx                                 ; Total number of bytes to store are now in EDX:EAX
    test    edx, edx
    jnz      .error                             ; Make it simple for now..
    mov     ecx, eax
    cld
    rep     movsb

    ; Enable paging
    ; First of all, make the first page blank
    pusha
    mov     edi, address_of_first_page
    mov     ecx, PAGE_SIZE / 4
    sub     eax, eax
    rep     stosd
    popa

    ;
    ; Populate the page tables
    ;
    PING(0, 0, '2020')
    ; 1) identity map the real-mode memory: PA = [0, 1MB]
    push    dword MEGA/PAGE_SIZE            ; page count for 1MB
    push    dword 0                         ; VA high part, constant=0 -- we're not going over a 4GB boundary
    push    dword 0                         ; VA low part
    push    dword 0                         ; PA high part
    push    dword 0                         ; PA low part
    call    MapPaToVaRange

    PING(0, 0, '3030')
    ; 2) identity map the PE image: PA = [IMAGE_BASE_PA, MAGE_BASE_PA+IMAGE_LENGTH)
    push    dword IMAGE_LENGTH/PAGE_SIZE    ; page count for the whole image
    push    dword 0                         ; VA high part
    push    dword IMAGE_BASE_PA             ; VA low part
    push    dword 0                         ; PA high part
    push    dword IMAGE_BASE_PA             ; PA low part
    call    MapPaToVaRange

    PING(0, 0, '4040')
    ; 3) map the PE image plus BITMAP_POOL plus let's say 2MB for PAGE pool,
    ; should be more than enought
    push    dword (IMAGE_LENGTH + BITMAPS_POOL_LENGTH + 2* MEGA)/PAGE_SIZE    ; page count for the whole image
    push    dword (IMAGE_BASE_VA64 >> 32)   ; VA high part
    push    dword 0                         ; VA low part - ImageBase is a multiple of 4GB => the low part is zero
    push    dword 0                         ; PA high part
    push    dword IMAGE_BASE_PA             ; PA low part
    call    MapPaToVaRange

    ;
    ; After all mapping operations, we are safe now to save the next pagine entry stucture we can use
    ;
    mov     edi, VA32(gAsmData)             ; NextPagingStructureEntry is the first entry in gAsmData structure
    mov     esi, VA32(address_of_next_page) ; Va where the last allocated paging structure entry addres is
    mov     ecx, 8                          ; sizeof(QWORD)
    cld
    rep     movsb



    ;
    ; Enable PAE, setup LME and paging
    ;
    PING(0, 0, '5050')
.enablePae:
    mov     eax, cr4
    or      eax, CR4_PAE            ; set bit 0x00000020
    mov     cr4, eax

    mov     ecx, IA32_EFER          ; MSR 0xC0000080, check out '9.8.5 Initializing IA-32e Mode' from Intel docs
    rdmsr                           ; also check out 'Table B-2. IA-32 Architectural MSRs' from Intel docs
    or      eax, IA23_EFER_LME      ; set LME bit, 0x100
    wrmsr

    ; enable paging
    mov     eax, address_of_first_page  ; get the PA of the root of the paging structures
    and     eax, PAGE_MASK              ; we don't use PCIDE/PWT/PCD, mask the lower 12 bits
    mov     cr3, eax                    ; set cr3

    mov     eax, cr0
    or      eax, CR0_PG
    mov     cr0, eax

    ;
    ; Load our own GDT and setup the 64-bit segments
    ;
    PING(0, 0, '4040')
    lgdt    [VA32(bootGdt)]
    mov     ax, SEL(data64)
    mov     ds, ax
    mov     es, ax
    mov     ss, ax
    mov     fs, ax
    mov     gs, ax

    push    dword SEL(code64)
    call    .pushEip

    ; load the CS with a ret far instruction
.pushEip:                                           ; at [esp] we have the return address (= $ = this address) and at [esp + 4] the wanted CS value
    add     dword [esp], EntryPoint64 - .pushEip    ; add how much is still needed to get to EntryPoint64
    retf                                            ; cs:eip <- far pointer from top of stack

.error:
    PING(80-4, 0, 'XXXX')
    cli
    hlt

EntryPoint64:
    [bits 64]
    ;
    ; Now that we're in 64 bits, prepare to enter the C code
    ;
    PING(0, 0, '6090')

    ; prepare the cusotm stack for the BSP in 64 bit mode
    ; this stack is at the end of the binary file
    mov      rsp, VA64(BSPStackStart)
    and      rsp, ALIGN_STACK_64

    ; execute the remaining code from the high-memory VA mapping
    mov     rax, VA64(.finalVa)
    jmp     rax

.finalVa:
    ; now we are using final 1T virtual addresses, full x64, have an small stack in place, so are ready to jump to our C code
    PING(0, 0, '7070')

    sub     rsp, 0x20
    mov     rcx, VA64(gAsmData)
    call    (KernelMain)
    add     rsp, 0x20

    PING(0, 0, '8080')
    hlt

;------------------------------------------------------------------------------------;

address_of_first_page   equ (IMAGE_BASE_PA + IMAGE_LENGTH + BITMAPS_POOL_LENGTH)
address_of_next_page    dq  (IMAGE_BASE_PA + IMAGE_LENGTH + BITMAPS_POOL_LENGTH)

; STDCALL CARRY_FLAG_BOOL MapPaToVaRange(__in QWORD Pa, __in QWORD Va, __in DWORD PageCount)
MapPaToVaRange:
    [bits 32]
    .Pa         equ  8      ; stackframe offset to PA
    .Va         equ  16     ; stackframe offset to VA
    .PageCount  equ  24     ; stackframe offset to PageCount

    push    ebp
    mov     ebp, esp
    pusha

    mov     ecx,    [ebp + .PageCount]

    .nextPage:
        push    dword   [ebp + .Va + 4]     ; VA high part
        push    dword   [ebp + .Va]         ; VA low part
        push    dword   [ebp + .Pa + 4]     ; PA high
        push    dword   [ebp + .Pa]         ; PA low
        call    MapPaToVaPage
        jc      .error

        ; advance both addresses with PAGE_SIZE
        add     dword   [ebp + .Va], PAGE_SIZE
        adc     dword   [ebp + .Va + 4], 0
        add     dword   [ebp + .Pa], PAGE_SIZE
        adc     dword   [ebp + .Pa + 4], 0
    loop    .nextPage

    ; clear carry flag and exit the function
    clc
    jmp     .done

.error:
    ; set carry flag and exit
    stc

.done:
    popa
    pop ebp
    ret 2*8 + 1*4

; STDCALL CARRY_FLAG_BOOL MapPaToVaPage(__in QWORD Pa, __in QWORD Va)
MapPaToVaPage:
    [bits 32]
    .Pa     equ  8      ; stackframe offset to PA
    .Va     equ  16     ; stackframe offset to VA

    push    ebp
    mov     ebp, esp
    pusha

    ; the layout of a 64-bit VA: |16: unused|9:PML4i|9:PDPTi|9:PDi|9:PTi|12 offset in page|
    ; and, we dont use offset in page. that field must be 0 when calling this function cuz
    ; we map only addresses align to page
    mov     edx, [ebp + .Va + 4]    ; high 32 bits of VA
    mov     ecx, [ebp + .Va]        ; low 32 bits of Va

    shld    edx, ecx, 16      ; get rif of first 16 unused
    shl     ecx, 16

    mov     ebx, address_of_first_page
    mov     eax, edx
    shld    edx, ecx, 9         ; drop 9 MSB bits of edx and transport 9 MSB from ecx to the 9 LSB of edx
    shl     ecx, 9              ; make the ecx change persistent
    shr     eax, (32-9)         ; get PML4 index
    shl     eax, 3              ; *8, each table entry is 8 bits in size
    add     ebx, eax            ; addres in table of the next entry

    mov     eax, [ebx]
    test    eax, eax

    jnz     .PDPTpresent

.PDPTnotPresent:
    ; Get a new page for our new paging structure
    mov     eax, [VA32(address_of_next_page)]
    add     eax, PAGE_SIZE
    mov     [VA32(address_of_next_page)], eax
    ; Fill with zero
    pusha
    mov     edi, eax
    mov     ecx, PAGE_SIZE / 4
    sub     eax, eax
    rep     stosd
    popa

    ; Link the new page at the entry [ebx]
    and     eax, PAGE_MASK
    or      eax, (VA_PRESENT|VA_WRITE_ACCESS|VA_USER_ACCESS)
    mov     [ebx], eax

.PDPTpresent:
    and     eax, PAGE_MASK
    mov     ebx, eax            ; At least to be consitent with code above
                                ; better way would be to reuse the code but im lazy to stay
                                ; and write reusable assembly code
    mov     eax, edx
    shld    edx, ecx, 9         ; drop 9 MSB bits of edx and transport 9 MSB from ecx to the 9 LSB of edx
    shl     ecx, 9              ; make the ecx change persistent
    shr     eax, (32-9)         ; get PML4 index
    shl     eax, 3              ; *8, each table entry is 8 bits in size
    add     ebx, eax            ; addres in table of the next entry

    mov     eax, [ebx]
    test    eax, eax

    jnz     .PDpresent

.PDnotPresent:
    ; Get a new page for our new paging structure
    mov     eax, [VA32(address_of_next_page)]
    add     eax, PAGE_SIZE
    mov     [VA32(address_of_next_page)], eax
    ; Fill with zero
    pusha
    mov     edi, eax
    mov     ecx, PAGE_SIZE / 4
    sub     eax, eax
    rep     stosd
    popa

    ; Link the new page at the entry [ebx]
    and     eax, PAGE_MASK
    or      eax, (VA_PRESENT|VA_WRITE_ACCESS|VA_USER_ACCESS)
    mov     [ebx], eax

.PDpresent:
    and     eax, PAGE_MASK
    mov     ebx, eax            ; At least to be consitent with code above
                                ; better way would be to reuse the code but im lazy to stay
                                ; and write reusable assembly code
    mov     eax, edx
    shld    edx, ecx, 9         ; drop 9 MSB bits of edx and transport 9 MSB from ecx to the 9 LSB of edx
    shl     ecx, 9              ; make the ecx change persistent
    shr     eax, (32-9)         ; get PML4 index
    shl     eax, 3              ; *8, each table entry is 8 bits in size
    add     ebx, eax            ; addres in table of the next entry

    mov     eax, [ebx]
    test    eax, eax

    jnz     .PTpresent

.PTnotPresent:
    ; Get a new page for our new paging structure
    mov     eax, [VA32(address_of_next_page)]
    add     eax, PAGE_SIZE
    mov     [VA32(address_of_next_page)], eax
    ; Fill with zero
    pusha
    mov     edi, eax
    mov     ecx, PAGE_SIZE / 4
    sub     eax, eax
    rep     stosd
    popa

    ; Link the new page at the entry [ebx]
    and     eax, PAGE_MASK
    or      eax, (VA_PRESENT|VA_WRITE_ACCESS|VA_USER_ACCESS)
    mov     [ebx], eax

.PTpresent:
    and     eax, PAGE_MASK
    mov     ebx, eax            ; At least to be consitent with code above
                                ; better way would be to reuse the code but im lazy to stay
                                ; and write reusable assembly code
    mov     eax, edx
    shld    edx, ecx, 9         ; drop 9 MSB bits of edx and transport 9 MSB from ecx to the 9 LSB of edx
    shl     ecx, 9              ; make the ecx change persistent
    shr     eax, (32-9)         ; get PML4 index
    shl     eax, 3              ; *8, each table entry is 8 bits in size
    add     ebx, eax            ; addres in table of the next entry

    ; got to the end of the page walk, edi points to the PTE where the actual PA should be filled in
    mov     eax, [ebp + .Pa + 4]
    mov     [ebx + 4], eax
    mov     eax, [ebp + .Pa]
    and     eax, PAGE_MASK
    or      eax, (VA_PRESENT|VA_WRITE_ACCESS|VA_USER_ACCESS)
    mov     [ebx], eax

    popa
    pop ebp
    ret 2*8


;-----------------------------------------------------------------------------------------;
;;
;; Define the data structure shared with (and needed by) the C code and instantiate it
;;
struc ASM_DATA
    .NextPagingStructureEntry   resb 0x8                        ; last allocated paging structure entry PA; address_of_next_page
    .E820MapNumberOfEntries:    resb 0x4                        ; the E820 memory map number of entries
    .E820Map                    resb 0x1B00                     ; the E820 memory map
endstruc
;; !!!!!!!!!! Keep this struct size in sync with ASM_DATA_MEM_ALLOCATED constant from global.h

gAsmData: istruc ASM_DATA
    times ASM_DATA_size db 0
iend

;;
;; We need our own gdt, providing at least 32 and 64 bits segments
;;
; prepare an initialized GDT containing all the descriptors we might need
bootGdt:
    .limit              dw  (.tableEnd - .tableStart) - 1
    .base               dd  PA(.tableStart)

    .tableStart:
        .zero           dq 0
        .code16         dq FLAT_DESCRIPTOR_CODE16
        .data16         dq FLAT_DESCRIPTOR_DATA16
        .code32         dq FLAT_DESCRIPTOR_CODE32
        .data32         dq FLAT_DESCRIPTOR_DATA32
        .code64         dq FLAT_DESCRIPTOR_CODE64
        .data64         dq FLAT_DESCRIPTOR_DATA64
    .tableEnd:
bootGdtEnd:

reloc:
.protected16:
    [bits 16]
    ; Load real mode IDT
    lidt    [REBASE(.idtReal)]

    ; Disable protected mode(PE)
    mov     eax, cr0
    and     eax, CR0_PE_DISABLE
	mov     cr0, eax

    ; Far jump to real mode
    jmp     0:REBASE(.real16)

.real16:
    ; Reload data segment registers with real mode values
    mov     ax, 0
    mov     ds, ax
    mov     es, ax
    mov     ss, ax
    mov     fs, ax
    mov     gs, ax

    ;;; REAL MODE HERE
    ; Setup a stack
    mov      sp, REAL_MODE_TOP_OF_STACK
    ; Enable interrupts
    sti

    .E820Start:
    ; Get memory map
    mov     di, REBASE(.firstEntry)     ; Where the memory map will start
    xor     ebx, ebx                    ; ebx must be 0 to start
    mov     DWORD [REBASE(.entries)], 0 ; Where we will keep the entries count
    mov     edx, SMAP_CODE              ; Place "SMAP" into edx
    mov     eax, 0x0000E820
    mov     [es:di + 20], DWORD 1       ; Force a valid ACPI 3.X entry
    mov     ecx, SMAP_ENTRY_SIZE        ; Ask for 24 bytes
    int     0x15
    jc      .error16                    ; CF = 1 -> unsupported function
    mov     edx, SMAP_CODE              ; Some BIOSes trash this register?
    cmp     eax, edx                    ; On success, eax must have been reset to "SMAP"
    jne     .error16
    test    ebx, ebx                    ; ebx = 0 implies list is only 1 entry long (worthless)
    je      .error16
    jmp     .testEntry

    .E820Continue:
    mov     eax, 0x0000E820
    mov     [es:di + 20], DWORD 1       ; Force a valid ACPI 3.X entry
    mov     ecx, SMAP_ENTRY_SIZE        ; Ask for 24 bytes again
    int     0x15
    jc      .E820Final                  ; Carry set means "end of list already reached"
    mov     edx, SMAP_CODE              ; Repair potentially trashed register

    .testEntry:
    jcxz    .nextEntry                  ; Skip any 0 length entries
    cmp     cl,SMAP_ENTRY_SIZE_COMMONLY ; Is this entry 24 byte or 20?
    jbe     .testEntryContinue
    test    BYTE [es:di + SMAP_ENTRY_SIZE_COMMONLY], 1  ; Little endian. We test here the last [7..0] bits from DWORD ACPI 3.0 Extended Attributes bitfield
    jz      .nextEntry

    .testEntryContinue:
    mov     ecx, [es:di + 8]            ; Get lower DWORD of memory region length
    or      ecx, [es:di + 12]           ; "or" it with upper DWORD to test for zero length
    jz      .nextEntry
    inc     DWORD [REBASE(.entries)]    ; Got a good entry: ++count, move to next entry
    add     di, SMAP_ENTRY_SIZE

    .nextEntry:
    test    ebx, ebx                    ; If ebx = 0, list is complete
    jne     .E820Continue

    .E820Final:
    clc                                 ; There is "jc" on end of list to this point, so the carry must be cleared
    ;
    ; Get back to protected32
    ;

    ; Disable interrupts
    cli

    ; Set PE(Protection Enable)
    mov     eax, cr0
    or      eax, CR0_PE_ENABLE
    mov     cr0, eax

    ; Far jump to protected32
    jmp     0x18:REBASE(.protected32)

    .error16
    hlt

.protected32:
    [bits 32]
    ; Load our own GDT
    lgdt    [REBASE(.ourGdt)]
    xor     ax, ax
    mov     ax, 0x20
    mov     ds, ax
    mov     es, ax
    mov     ss, ax
    mov     fs, ax
    mov     gs, ax

    ; Restore IDT
    lidt    [REBASE(.savedIdt)]

    ; Get back to 32 bits code from 32MB
    mov     eax, VA32(ASMEntryPoint.restorePointFrom16)
    jmp     eax

    .error
    hlt

    ; IDT table pointer for 16bit access
    .idtReal:
		dw 0x03FF                               ; table limit (size)
		dd 0x00000000                           ; table base address

    ; prepare an initialized GDT containing all the descriptors we might need
    .ourGdt:
        .limit              dw  (.tableEnd - .tableStart) - 1
        .base               dd  REBASE(.tableStart)

        .tableStart:
            .zero           dq 0
            .code16         dq FLAT_DESCRIPTOR_CODE16
            .data16         dq FLAT_DESCRIPTOR_DATA16
            .code32         dq FLAT_DESCRIPTOR_CODE32
            .data32         dq FLAT_DESCRIPTOR_DATA32
            .code64         dq FLAT_DESCRIPTOR_CODE64
            .data64         dq FLAT_DESCRIPTOR_DATA64
        .tableEnd:
    .ourGdtEnd:

    .savedIdt:
        .limitIDT           dw  0
        .baseIDT            dd  0
    .savedIdtEnd:

    .mmap:
        .entries            dd  0   ; Number of entries in memory map
        .firstEntry                 ; Starting from this address, we will save memory map from INT15
relocEnd:

BSPStackEnd:
    TIMES   INITIAL_BPS_STACK_SIZE  db  0
    align   0x40
BSPStackStart: