#include "lib/crt.h"
#include "io/vga.h"

/* Useful constants */
#define MAX_DIGITS  32
#define INT_BASE    10
#define HEXA_BASE   16

/* Static functions */
#pragma function(memcpy)
#pragma function(memset)
void * __cdecl memcpy(void *Dest, const void *Src, size_t Length);
void* __cdecl memset(void* _Dst, int _Val, size_t _Size);

__forceinline static void Swap(CHAR* a, CHAR* b);
__forceinline static void ReverseString(CHAR* Str, size_t Length);

/* Implementations */
/*1*/
void *
crt_memcpy(
    void *Dest,
    const void *Src,
    size_t Length
)
{
    return memcpy(Dest, Src, Length);
}

/*2*/
size_t
crt_strlen(
    const CHAR* str
)
{
    const CHAR* endOfString = str;

    if (str == NULL)
    {
        return 0;
    }

    while (*endOfString)
    {
        endOfString++;
    }

    return endOfString - str;
}

/*3*/
CHAR*
crt_itoa(
    INT64 Num,
    CHAR* Str,
    BYTE Base
)
{
    BYTE i = 0;
    BYTE isNegative = FALSE;

    if (Str == NULL)
    {
        return NULL;
    }

    // Handle 0 explicitely, otherwise empty string is printed
    if (Num == 0)
    {
        Str[i++] = '0';
        Str[i] = '\0';
        return Str;
    }

    // Negative numbers are handled only with base 10.
    // Otherwise numbers are considered unsigned.
    if (Num < 0 && Base == 10)
    {
        isNegative = TRUE;
        Num = -Num;
    }

    // Process individual digits
    while (Num != 0)
    {
        WORD rem = Num % Base;
        Str[i++] = (rem > 9) ? (CHAR)((rem - 10) + 'a') : (CHAR)(rem + '0');
        Num = Num / Base;
    }

    // If number is negative, append '-'
    if (isNegative)
        Str[i++] = '-';

    // Append string terminator
    Str[i] = '\0';

    // Reverse the string
    ReverseString(Str, i);

    return Str;
}

/*4*/
CHAR*
crt_uitoa(
    QWORD Num,
    CHAR* Str,
    BYTE Base
)
{
    BYTE i = 0;

    if (Str == NULL)
    {
        return NULL;
    }

    // Handle 0 explicitely, otherwise empty string is printed
    if (Num == 0)
    {
        Str[i++] = '0';
        Str[i] = '\0';
        return Str;
    }

    // Process individual digits
    while (Num != 0)
    {
        WORD rem = Num % Base;
        Str[i++] = (rem > 9) ? (CHAR)((rem - 10) + 'a') : (CHAR)(rem + '0');
        Num = Num / Base;
    }

    // Append string terminator
    Str[i] = '\0';

    // Reverse the string
    ReverseString(Str, i);

    return Str;
}

/*5*/
void
crt_sprintf(
    CHAR* Buffer,
    const CHAR *Format,
    ...
)
{
    DWORD i = 0;
    CHAR c = 0x00;
    CHAR **arg = (CHAR **)&Format;
    CHAR buf[MAX_DIGITS] = { 0 };

    arg++;

    while ((c = *Format++) != 0)
    {
        if (c != '%')
        {
            Buffer[i] = c;
            i++;
        }
        else
        {
            CHAR *p = NULL;
            c = *Format++;

            switch (c)
            {
            case 'd':
                p = crt_itoa(*((INT64 *)arg++), buf, INT_BASE);
                goto string;
                break;

            case 'u':
                p = crt_uitoa(*((QWORD *)arg++), buf, INT_BASE);
                goto string;
                break;

            case 'x':
                p = crt_uitoa(*((QWORD *)arg++), buf, HEXA_BASE);
                goto string;
                break;

            case 's':
                p = *arg++;
                if (!p)
                {
                    p = (CHAR*)"(null)";
                }

            string:
                while (*p)
                {
                    Buffer[i] = (*p++);
                    i++;
                }
                break;

            default:
                Buffer[i] = *((CHAR *)arg++);
                i++;
                break;
            }
        }
    }

    Buffer[i] = 0;

    return;
}

/*7*/
void*
crt_memset(
    void    *Dest,
    int     Value,
    size_t  Length
)
{
    return memset(Dest, Value, Length);
}

/*8*/ 
int       
crt_strcmp(
    const char* string1,
    const char* string2
)
{
    for (int i = 0; ; i++)
    {
        if (string1[i] != string2[i])
        {
            return string1[i] < string2[i] ? -1 : 1;
        }

        if (string1[i] == '\0')
        {
            return 0;
        }
    }
}

/* Static functions */
void *
memcpy(
    void *Dest,
    const void *Src,
    size_t Length
)
{
    PBYTE d = (PBYTE)Dest;
    PBYTE s = (PBYTE)Src;

    if (
        (Dest == NULL)
        || (Src == NULL)
        )
    {
        return NULL;
    }

    while (Length--)
    {
        (*d) = (*s);
        ++d;
        ++s;
    }

    return Dest;
}

void*
__cdecl
memset(
    void*   _Dst,
    int     _Val,
    size_t  _Size)
{
    BYTE* p = (BYTE*)_Dst;

    while (_Size--)
    {
        (*p) = (BYTE)_Val;
        p++;
    }

    return _Dst;
}

__forceinline
static void
Swap(
    CHAR* a,
    CHAR* b
)
{
    CHAR temp = *a;
    *a = *b;
    *b = temp;
}

__forceinline
static void
ReverseString(
    CHAR* Str,
    size_t Length
)
{
    size_t start = 0;
    size_t end = Length - 1;

    while (start < end)
    {
        Swap((Str + start), (Str + end));
        start++;
        end--;
    }
}