#include "lib/bitmap.h"

/* Number of bits in an element. */
#define ELEM_BITS (sizeof (BitmapElemType) * 8)

/* Static functions */
static void                         _BitmapMark(BITMAP* Bitmap, QWORD BitIdx);
static void                         _BitmapReset(BITMAP* Bitmap, QWORD BitIdx);
__forceinline static QWORD          _ElemIdx(QWORD BitIdx);
__forceinline static BitmapElemType _BitMask(QWORD BitIdx);
__forceinline static DWORD          _NumberOfBytes(QWORD NumberOfBits);
__forceinline static DWORD          _ElemCnt(QWORD NumberOfBits);

/* Implementations */
void            
BitmapEarlyInit(
    BITMAP* Bitmap,
    QWORD   BaseAddress,
    QWORD   NumberOfBits
)
{
    Bitmap->Bitmap = (BitmapElemType*)BaseAddress;
    Bitmap->NumberOfBits = NumberOfBits;
    BitmapSetAll(Bitmap, BIT_STATUS_FREE);
    
    return;
}

/* Creation and destruction */
BITMAP* 
BitmapCreate(
    QWORD NumberOfBits
)
{
    UNREFERENCED_PARAMETER(NumberOfBits);
    return NULL;
}

STATUS 
BitmapDestroy(
    BITMAP* Bitmap
)
{
    // TODO: implement
    UNREFERENCED_PARAMETER(Bitmap);
    return STATUS_NOT_IMPLEMENTED;
}

/* Setting and testing single bits. */
STATUS 
BitmapSet(
    BITMAP*     Bitmap,
    QWORD       Index,
    BIT_STATUS  Value
)
{
    if (!Bitmap) { return STATUS_INVALID_PARAMETER_1; }

    if (Index >= Bitmap->NumberOfBits) { return STATUS_INVALID_PARAMETER_2; }

    if (Value == BIT_STATUS_USED)
    {
        _BitmapMark(Bitmap, Index);
    }
    else
    {
        _BitmapReset(Bitmap, Index);
    }

    return STATUS_SUCCESS;
}

BOOLEAN 
BitmapTest(
    BITMAP* Bitmap,
    QWORD   Index
)
{
    if (!Bitmap) { return FALSE; }

    if (Index >= Bitmap->NumberOfBits) { return FALSE; }

    return (Bitmap->Bitmap[_ElemIdx(Index)] & _BitMask(Index)) != 0;

}

/* Setting and testing multiple bits. */
STATUS 
BitmapSetAll(
    BITMAP*     Bitmap,
    BIT_STATUS  Value
)
{
    return BitmapSetMultiple(
        Bitmap,
        0,
        Bitmap->NumberOfBits,
        Value
    );
}

STATUS 
BitmapSetMultiple(
    BITMAP*     Bitmap,
    QWORD       Start,
    QWORD       Cnt,
    BIT_STATUS  Value
)
{
    if (!Bitmap) { return STATUS_INVALID_PARAMETER_1; }

    if (Start + Cnt > Bitmap->NumberOfBits) { return STATUS_INVALID_PARAMETERS; }

    for (QWORD i = 0; i < Cnt; i++)
    {
        BitmapSet(Bitmap, Start + i, Value);
    }

    return STATUS_SUCCESS;
}

STATUS
BitmapScanMultiple(
    BITMAP      *Bitmap,
    QWORD       Cnt,
    BIT_STATUS  Value,
    QWORD       *Index
)
{
    if (!Bitmap) { return STATUS_INVALID_PARAMETER_1; }

    if (Cnt > Bitmap->NumberOfBits) { return STATUS_INVALID_PARAMETER_2; }

    QWORD index = 0;
    BOOLEAN found = FALSE;
    QWORD last = Bitmap->NumberOfBits - Cnt;
    for (index = 0; index <= last; index++)
    {
        found = TRUE;
        for (QWORD j = index; j < index + Cnt; j++)
        {
            if (BitmapTest(Bitmap, j) != Value)
            {
                found = FALSE;
                break;
            }
        }

        if (found)
        {
            goto end;
        }
    }

end:
    if (found)
    {
        *Index = index;
        return STATUS_SUCCESS;
    }

    return STATUS_BITMAP_FULL;
}

/* Static functions */
/* Sets the bit numbered BIT_IDX in B to true. */
static 
void 
_BitmapMark(
    BITMAP* Bitmap,
    QWORD   BitIdx
)
{
    QWORD idx = _ElemIdx(BitIdx);
    BitmapElemType mask = _BitMask(BitIdx);

    Bitmap->Bitmap[idx] |= mask;

    return;
}

/* Sets the bit numbered BIT_IDX in B to false. */
static 
void 
_BitmapReset(
    BITMAP* Bitmap,
    QWORD   BitIdx
)
{
    QWORD idx = _ElemIdx(BitIdx);
    BitmapElemType mask = _BitMask(BitIdx);

    Bitmap->Bitmap[idx] &= ~mask;

    return;
}

/* Returns the index of the element that contains the bit
numbered BitIdx. */
__forceinline 
static 
QWORD 
_ElemIdx(
    QWORD BitIdx
)
{
    return BitIdx / ELEM_BITS;
}

/* Returns an BitmapElemType where only the bit corresponding to
BitIdx is turned on. */
__forceinline 
static 
BitmapElemType 
_BitMask(
    QWORD BitIdx
)
{
    return (BitmapElemType)1 << (BitIdx % ELEM_BITS);
}

/* Returns the number of bytes required for NumberOfBits bits. */
__forceinline 
static 
DWORD
_NumberOfBytes(
    QWORD NumberOfBits
)
{
    return (sizeof(BitmapElemType) * _ElemCnt(NumberOfBits));
}

/* Returns the number of elements required for NumberOfBits bits. */
__forceinline 
static 
DWORD
_ElemCnt(
    QWORD NumberOfBits
)
{
    return (DWORD)(DIV_ROUND_UP(NumberOfBits, ELEM_BITS));
}