#include "global/global.h"

KERNEL_GLOBAL_DATA KernelGlobalData;

STATUS
KernelGlobalDataInit(
    ASM_DATA* AsmData
)
{
    STATUS status = STATUS_SUCCESS;

    status = SpinlockInit(&KernelGlobalData.Logging.Spinlock);
    if (!SUCCESS(status))
    {
        return status;
    }

    KernelGlobalData.AsmData = AsmData;

    return status;
}