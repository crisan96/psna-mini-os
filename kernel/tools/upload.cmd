:: - define your PXE dir below (case sensitive!!), if you want automatic PXE upload
if /I [%computername%]==[MVANTA-LPT] 		set PXE_DIR=pxe-mvanta
if /I [%computername%]==[ACRISAN-LPT] 		set PXE_DIR=pxe-acrisan
:: - ...................................................................

copy c:\Users\acrisan\Documents\Hypervisor\GIT\MiniHV\.bin\x64\Release\minihv.bin C:\Vm\tftpfolder


if [%PXE_DIR%]==[] goto no_pxe
winscp /command "open ftp://tftp:tftp@pxe.tstpkr.exdc02.bitdefender.biz -passive=on" "option confirm off" "put %1 %PXE_DIR%/pxeboot.bin" "put %2 %PXE_DIR%/gpxe" "exit"
goto done

:no_pxe
echo PXE dir not configured (edit upload.cmd if needed), the resulted binary wasn't uploaded!

:done
